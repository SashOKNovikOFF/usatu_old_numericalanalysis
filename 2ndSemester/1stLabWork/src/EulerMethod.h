// C++ includes
#include <vector>
#include <string>

#ifndef EULERMETHOD_H
#define EULERMETHOD_H

class EulerMethod
{
protected:	
	double step;
	std::vector<double> borderValues;
	std::vector<double> initialValues;

	double getFunctionValue(double x, double y_1, double y_2);
public:
	EulerMethod();
	EulerMethod(double step, std::vector<double>& borderValues, std::vector<double>& initalValues);
	void getSolution(std::string fileName);
};

#endif // EULERMETHOD_H 