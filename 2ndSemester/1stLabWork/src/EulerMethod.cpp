// Related headers
#include "EulerMethod.h"

// C++ includes
#include <fstream>
#include <iomanip>
#include <cmath>

EulerMethod::EulerMethod()
{
	step = 0.1;
	
	borderValues.resize(2);
	borderValues[0] = 0.0;
	borderValues[1] = 3.0;

	initialValues.resize(2);
	initialValues[0] = 0.0;
	initialValues[1] = 1.0 / 6.0;
};

EulerMethod::EulerMethod(double step_, std::vector<double>& borderValues_, std::vector<double>& initalValues_)
{
	step = step_;
	borderValues = borderValues_;
	initialValues = initalValues_;
};

void EulerMethod::getSolution(std::string fileName)
{
	std::vector<double> solution(2, 0);
	solution[0] = initialValues[0];
	solution[1] = initialValues[1];

	std::fstream resultFile(fileName, std::ifstream::out);
	resultFile << "Results of computation" << std::endl;
	resultFile << "OX\tY_1\tY_2" << std::endl;
	resultFile << std::fixed << std::setprecision(2) << borderValues[0] << std::setprecision(8) << "\t" << solution[0] << "\t" << solution[1] << std::endl;

	int countNodes = (int)ceil((borderValues[1] - borderValues[0]) / step);
	for (int node = 1; node <= countNodes; node++)
	{
		double lastValue = solution[0];
		double x = borderValues[0] + node * step;

		solution[0] = solution[0] + step * solution[1];
		solution[1] = solution[1] + step * getFunctionValue(x, 0, 0);

		resultFile << std::setprecision(2) << x << std::setprecision(8) << "\t" << solution[0] << "\t" << solution[1] << std::endl;
	};

	resultFile.close();
};

double EulerMethod::getFunctionValue(double x, double y_1, double y_2)
{
	return -0.05 * exp(-0.6 * x);
};