// C++ includes
#include <fstream>
#include <iomanip>
#include <cmath>
#include <string>

#ifndef ANALYTICSOLUTION_H
#define ANALYTICSOLUTION_H

void getAnalyticSolution(std::string fileName)
{
	std::fstream resultFile(fileName, std::ifstream::out);
	resultFile << "Results of computation" << std::endl;
	resultFile << "OX\tY_1\tY_2" << std::endl;

	int countNodes = (int)ceil((3.0 - 0.0) / 0.1);
	for (int node = 1; node <= countNodes; node++)
	{
		double x = 0.0 + node * 0.1;

		double solution0 = - 5.0 / 36.0 * exp(-3.0 / 5.0 * x) + 1.0 / 12.0 * x + 5.0 / 36.0;
		double solution1 = 1.0 / 12.0 * exp(-3.0 / 5.0 * x) + 1.0 / 12.0;

		resultFile << std::setprecision(2) << x << std::setprecision(8) << "\t" << solution0 << "\t" << solution1 << std::endl;
	};

	resultFile.close();
};

#endif // ANALYTICSOLUTION_H