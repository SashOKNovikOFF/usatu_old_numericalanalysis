// User includes
#include "EulerMethod.h"

#ifndef RUNGEKUTTAMETHOD_H
#define RUNGEKUTTAMETHOD_H

class RungeKuttaMethod : public EulerMethod
{
public:
	RungeKuttaMethod();
	RungeKuttaMethod(double step_, std::vector<double>& borderValues_, std::vector<double>& initalValues_);
	void getSolution(std::string fileName);
};

#endif // RUNGEKUTTAMETHOD_H