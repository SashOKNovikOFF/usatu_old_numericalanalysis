// C++ includes
#include <string>

// User headers
#include "EulerMethod.h"
#include "RungeKuttaMethod.h"
#include "AnalyticSolution.h"

int main()
{
	EulerMethod eulerObject;
	eulerObject.getSolution("EulerResult.txt");
	RungeKuttaMethod rungeObject;
	rungeObject.getSolution("RungeKuttaResult.txt");
	getAnalyticSolution("AnalyticResult.txt");

	return 0;
};