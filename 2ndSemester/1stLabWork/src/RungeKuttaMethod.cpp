// Related headers
#include "RungeKuttaMethod.h"

// C++ includes
#include <fstream>
#include <iomanip>
#include <cmath>

RungeKuttaMethod::RungeKuttaMethod() : EulerMethod()
{

};

RungeKuttaMethod::RungeKuttaMethod(double step_, std::vector<double>& borderValues_, std::vector<double>& initalValues_) : EulerMethod(step_, borderValues_, initalValues_)
{

};

void RungeKuttaMethod::getSolution(std::string fileName)
{
	std::vector<double> solution(2, 0);
	solution[0] = initialValues[0];
	solution[1] = initialValues[1];

	std::fstream resultFile(fileName, std::ifstream::out);
	resultFile << "Results of computation" << std::endl;
	resultFile << "OX\tY_1\tY_2" << std::endl;
	resultFile << std::fixed << std::setprecision(2) << borderValues[0] << std::setprecision(8) << "\t" << solution[0] << "\t" << solution[1] << std::endl;

	double k[4], q[4]; // ������������ ������ �����-�����
	
	int countNodes = (int)ceil((borderValues[1] - borderValues[0]) / step);
	for (int node = 1; node <= countNodes; node++)
	{
		double x = borderValues[0] + node * step;

		q[0] = getFunctionValue(x, 0, 0);
		q[1] = getFunctionValue(x + step / 2, 0, 0);
		q[2] = getFunctionValue(x + step / 2, 0, 0);
		q[3] = getFunctionValue(x + step, 0, 0);

		k[0] = solution[1];
		k[1] = solution[1] + step * q[1] / 2;
		k[2] = solution[1] + step * q[2] / 2;
		k[3] = solution[1] + step * q[3];

		solution[0] = solution[0] + step / 6 * (k[0] + 2 * k[1] + 2 * k[2] + k[3]);
		solution[1] = solution[1] + step / 6 * (q[0] + 2 * q[1] + 2 * q[2] + q[3]);

		resultFile << std::setprecision(2) << x << std::setprecision(8) << "\t" << solution[0] << "\t" << solution[1] << std::endl;
	};

	resultFile.close();
};