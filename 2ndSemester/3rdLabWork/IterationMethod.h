#ifndef ITERATIONMETHOD_H
#define ITERATIONMETHOD_H

// C++ includes
#include <string>

class IterationMethod                                        // ����������� ����������� ����� ���������� ������
{
private:
	int nodesNumber;                                         //!< ����� �����
	double step;                                             //!< ��� �����
    double iterationParameter;                               //!< ������������ ��������
	
	double** lastValue;                                      //!< ��������� ����������� �������� �� ����� �����
	double** currentValue;                                   //!< ������� ����������� �������� �� ����� �����
    double** residual;                                       //!< �������� ������� �� �����
    double** operatorResidual;                               //!< �������� ����������� ���������, ���������������� �� �������, �� �����

    double getInnerValue(double** array, int x, int y);      //!< ��������� �������� ������ �����
    
    double getLBorderValue(double** array, int y);           //!< ��������� �������� �� ����� ������� �����
    double getRBorderValue(double** array, int y);           //!< ��������� �������� �� ������ ������� �����
    double getDBorderValue(double** array, int x);           //!< ��������� �������� �� ������ ������� �����
    double getUBorderValue(double** array, int x);           //!< ��������� �������� �� ������� ������� �����
    
    double getLDAngleValue(double** array);                  //!< ��������� �������� � ����� ������ ���� �����
    double getRDAngleValue(double** array);                  //!< ��������� �������� � ������ ������ ���� �����
    double getLUAngleValue(double** array);                  //!< ��������� �������� � ����� ������� ���� �����
    double getRUAngleValue(double** array);                  //!< ��������� �������� � ������ ������� ���� �����
    
    double getGridValue(double** array, int x, int y);       //!< ��������� �������� � ���� �����
    
	double getScalarProduct(double** x, double** y);         //!< ��������� ��������� ������������

	const static double epsilon;                             //!< ��������, � ������� ������ ������������ ��������� �������
public:
	IterationMethod();                                       //!< ����������� (����� ����� - 11)
	IterationMethod(int nodesNumber_);                       //!< ����������� � ����������� (����� ����� - iteration_)
	~IterationMethod();                                      //!< ����������
	
	double getFunctionValue(double x, double y);             //!< �������� �������� ������� f(x, y)
	double getAnalyticValue(double x, double y);             //!< �������� ������� ������ ������� � ����� (x, y)
    
	void runOneStep();                                       //!< ��������� ���� ��� ��������
    void correctValue();                                     //!< ��������������� ��������� ��������

	bool checkResults();                                     //!< ��������� ���������� ��������
	void saveResults(std::string fileName);                  //!< ��������� ���������� ��������
};

#endif // ITERATIONMETHOD_H