#define _USE_MATH_DEFINES

// Relative headers
#include "IterationMethod.h"

// C++ includes
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>

const double IterationMethod::epsilon = 0.0001;

IterationMethod::IterationMethod()
{
	nodesNumber = 11;
	step = 1.0 / (nodesNumber - 1);
	
	lastValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	
    residual = new double* [nodesNumber];
	operatorResidual = new double* [nodesNumber];
	
    for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
		
        residual[node] = new double [nodesNumber];
		operatorResidual[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0.;
			currentValue[x][y] = 0.;

            residual[x][y] = 0.;
            operatorResidual[x][y] = 0.;
		};
};
IterationMethod::IterationMethod(int nodesNumber_)
{
	nodesNumber = nodesNumber_;
	step = 1.0 / (nodesNumber_ - 1);
	
	lastValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	
    residual = new double* [nodesNumber];
	operatorResidual = new double* [nodesNumber];
	
    for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
		
        residual[node] = new double [nodesNumber];
		operatorResidual[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0.;
			currentValue[x][y] = 0.;

            residual[x][y] = 0.;
            operatorResidual[x][y] = 0.;
		};
};
IterationMethod::~IterationMethod()
{

};

double IterationMethod::getInnerValue(double** array, int x, int y)
{
    return (4 * array[x][y] - array[x - 1][y] - array[x + 1][y] - array[x][y - 1] - array[x][y + 1]) / step / step;
};

double IterationMethod::getLBorderValue(double** array, int y)
{
    return (4 * array[0][y] - 2 * array[1][y] - array[0][y + 1] - array[0][y - 1]) / step / step;
};
double IterationMethod::getRBorderValue(double** array, int y)
{
    return (4 * array[nodesNumber - 1][y] - 2 * array[nodesNumber - 2][y] - array[nodesNumber - 1][y - 1] - array[nodesNumber - 1][y + 1]) / step / step;
};
double IterationMethod::getDBorderValue(double** array, int x)
{
    return (4 * array[x][0] - 2 * array[x][1] - array[x - 1][0] - array[x + 1][0]) / step / step;
};
double IterationMethod::getUBorderValue(double** array, int x)
{
    return (4 * array[x][nodesNumber - 1] - 2 * array[x][nodesNumber - 2] - array[x - 1][nodesNumber - 1] - array[x + 1][nodesNumber - 1]) / step / step;
};

double IterationMethod::getLDAngleValue(double** array)
{
    return (4 * array[0][0] - 2 * array[1][0] - 2 * array[0][1]) / step / step;
};
double IterationMethod::getRDAngleValue(double** array)
{
    return (4 * array[nodesNumber - 1][0] - 2 * array[nodesNumber - 2][0] - 2 * array[nodesNumber - 1][1]) / step / step;
};
double IterationMethod::getLUAngleValue(double** array)
{
    return (4 * array[0][nodesNumber - 1] - 2 * array[0][nodesNumber - 2] - 2 * array[1][nodesNumber - 1]) / step / step;
};
double IterationMethod::getRUAngleValue(double** array)
{
    return (4 * array[nodesNumber - 1][nodesNumber - 1] - 2 * array[nodesNumber - 2][nodesNumber - 1] - 2 * array[nodesNumber - 1][nodesNumber - 2]) / step / step;
};

double IterationMethod::getGridValue(double** array, int x, int y)
{
    if ((x == 0) && (y == 0))
        return getLDAngleValue(array);
    if ((x == 0) && (y == (nodesNumber - 1)))
        return getLUAngleValue(array);
    if ((x == (nodesNumber - 1)) && (y == 0))
        return getRDAngleValue(array);
    if ((x == (nodesNumber - 1)) && (y == (nodesNumber - 1)))
        return getRUAngleValue(array);
    
    if (x == 0)
        return getLBorderValue(array, y);
    if (x == (nodesNumber - 1))
        return getRBorderValue(array, y);
    if (y == 0)
        return getDBorderValue(array, x);
    if (y == (nodesNumber - 1))
        return getUBorderValue(array, x);
    
    return getInnerValue(array, x, y);
}

double IterationMethod::getScalarProduct(double** x, double** y)
{
	double tempSum = 0.;
	for (int i = 0; i < nodesNumber; i++)
		for (int j = 0; j < nodesNumber; j++)
			tempSum += step * step * x[i][j] * y[i][j];

	return tempSum;
};

double IterationMethod::getFunctionValue(double x, double y)
{
    return 2. * (x - y);
}
double IterationMethod::getAnalyticValue(double x, double y)
{
    return (x * x - y * y) / 2 + (y * y * y - x * x * x) / 3;
};

void IterationMethod::runOneStep()
{
    double tempNum = 0.;
    double tempDenom = 0.;
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
            residual[x][y] = getGridValue(lastValue, x, y) - getFunctionValue(x * step, y * step);

	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
            operatorResidual[x][y] = getGridValue(residual, x, y);
    
	iterationParameter = getScalarProduct(residual, residual) / getScalarProduct(operatorResidual, residual);

    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            currentValue[x][y] = lastValue[x][y] - iterationParameter * residual[x][y];
    
	double** temp;
	temp         = lastValue;
	lastValue    = currentValue;
	currentValue = temp;
}
void IterationMethod::correctValue()
{
    double tempSum = 0.;
    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            tempSum += currentValue[x][y];

    tempSum /= (nodesNumber + 1) * (nodesNumber + 1);

    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            currentValue[x][y] -= tempSum;
};

bool IterationMethod::checkResults()
{
	double maxValue = std::abs(currentValue[0][0] - lastValue[0][0]);
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
			if (maxValue < std::abs(currentValue[x][y] - lastValue[x][y]))
				maxValue = std::abs(currentValue[x][y] - lastValue[x][y]);
	
	if (maxValue < epsilon)
		return true;
	else
		return false;
};
void IterationMethod::saveResults(std::string fileName)
{
	std::fstream LIFile(fileName + "_LastIteration.txt", std::ifstream::out);
	std::fstream CIFile(fileName + "_CurrentIteration.txt", std::ifstream::out);
	std::fstream ASFile(fileName + "_AnalyticSolution.txt", std::ifstream::out);
    std::fstream ErrorFile(fileName + "_MeasureErrors.txt", std::ifstream::out);
    std::fstream ResidualFile(fileName + "_Residal.txt", std::ifstream::out);
	
	LIFile << "Results of computation:" << std::endl;
	CIFile << "Results of computation:" << std::endl;
	ASFile << "Results of computation:" << std::endl;
    ErrorFile << "Results of computation:" << std::endl;
    ResidualFile << "Results of computation:" << std::endl;

	for (int x = nodesNumber - 1; x >= 0; x--)
	{
		for (int y = 0; y < nodesNumber; y++)
		{
			LIFile << lastValue[x][y] << "\t";
			CIFile << currentValue[x][y] << "\t";
			ASFile << getAnalyticValue(x * step, y * step) << "\t";
            ErrorFile << currentValue[x][y] - getAnalyticValue(x * step, y * step) << "\t";
            ResidualFile << residual[x][y] << "\t";
		};
		LIFile << std::endl;
		CIFile << std::endl;
		ASFile << std::endl;
        ErrorFile << std::endl;
        ResidualFile << std::endl;
	};
};