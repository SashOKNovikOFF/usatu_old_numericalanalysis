// User includes
#include "IterationMethod.h"

int main()
{
	const int nodesNumber = 11;
	const double step = 1.0 / (nodesNumber - 1);
	const int iterationNumber = 100;

	IterationMethod iterationObject(nodesNumber);
	
	bool flag = false;
	while (!flag)
	{
        iterationObject.correctValue();
		for (int iteration = 1; iteration <= iterationNumber; iteration++)
		{
			iterationObject.runOneStep();
			flag = iterationObject.checkResults();
		};
	};

	iterationObject.saveResults("1st");

	return 0;
};