#ifndef ITERATION_H
#define ITERATION_H

class Iteration                                    // ��������
{
protected:
	double gridLength;                             //!< ����� ���������� �����
	double step;                                   //!< ��� �����
	
	double leftBorder;                             //!< ������ ������� ��������� A
	double rightBorder;                            //!< ������� ������� ��������� A

	double coeff_A;
	double coeff_B;
	double coeff_R;
	double coeff_S;
	double coeff_T;
	double coeff_ETA;
	int coeff_N;
	double coeff_Q;

	double getCoeff_X(int iteration);            //!< �������� ����������� x_j

	const static double epsilon;                 //!< ��������, � ������� ������ ������������ ��������� �������
public:
	Iteration();                                 //!< �����������
												 //   (����� ���������� ����� - 1,
	                                             //    ��� ����� - 0.1)
	Iteration(double gridLength_, double step_); //!< ����������� � �����������
												 //   (����� ���������� ����� - gridLength_,
												 //    ��� ����� - step_)
	~Iteration();                                //!< ����������
	
	int getCoeff_N();                          //!< �������� ���������� ��������
	virtual double getIterator_1(int iteration);       //!< �������� ������������ �������� 1
    virtual double getIterator_2(int iteration);       //!< �������� ������������ �������� 2
};

#endif // ITERATION_H