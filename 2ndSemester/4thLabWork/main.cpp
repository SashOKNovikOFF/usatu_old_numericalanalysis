// User includes
#include "IterationSimple.h"
#include "IterationMethod.h"

int main()
{
	const int nodesNumber = 11;
	const double gridLength = 1.;
	const double step = gridLength / (nodesNumber - 1);

	IterationSimple parameter(gridLength, step);
	IterationMethod iterationObject(nodesNumber, gridLength, parameter);
	
	bool flag = false;
	while (!flag)
	{
		iterationObject.runOneStep(0);
		flag = iterationObject.checkResults();
	};

	iterationObject.saveResults("1st");

	return 0;
};