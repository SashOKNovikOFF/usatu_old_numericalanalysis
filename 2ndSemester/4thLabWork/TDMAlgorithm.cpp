// Related headers
#include "TDMAlgorithm.h"

TDMAlgorithm::TDMAlgorithm()
{

};

void TDMAlgorithm::getSolution(double** matrix, double* solution, int size)
{   
    double* ksi = new double [size + 1];
	double* eta = new double [size + 1];
    ksi[0] = eta[0] = 0.;

    for (int i = 0; i < size; i++)
    {
        ksi[i + 1] = matrix[2][i] / (- matrix[1][i] - matrix[0][i] * ksi[i]);
        eta[i + 1] = (matrix[0][i] * eta[i] - matrix[3][i]) / (- matrix[1][i] - matrix[0][i] * ksi[i]);
    };

	solution[size - 1] = ksi[size] * 0. + eta[size];
    for (int i = size - 1; i > 0; i--)
        solution[i - 1] = ksi[i] * solution[i] + eta[i];

	delete ksi;
	delete eta;
};
