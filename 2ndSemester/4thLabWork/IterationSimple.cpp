#define _USE_MATH_DEFINES

// Relative includes
#include "IterationSimple.h"

// C++ includes
#include <cmath>

IterationSimple::IterationSimple() : Iteration()
{

};
IterationSimple::IterationSimple(double gridLength_, double step_) : Iteration(gridLength_, step_)
{

};
IterationSimple::~IterationSimple()
{

};

int IterationSimple::getCoeff_N()
{
	return (int)ceil(0.1 * log(4. / pow(tan(M_PI * step / 2. / gridLength), 2)) * log(4. / epsilon));
};
double IterationSimple::getIterator_1(int iteration)
{
	return 2. / step / step * sin(M_PI * step / gridLength);
};
double IterationSimple::getIterator_2(int iteration)
{
	return 2. / step / step * sin(M_PI * step / gridLength);
};