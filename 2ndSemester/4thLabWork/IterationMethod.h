#ifndef ITERATIONMETHOD_H
#define ITERATIONMETHOD_H

// Relative includes
#include "IterationSimple.h"

// User includes
#include "TDMAlgorithm.h"

// C++ includes
#include <string>

class IterationMethod                                        // ����� ���������� �����������
{
private:
	int nodesNumber;                                         //!< ����� �����
	double gridLength;                                       //!< ����� ���������� �����
	double step;                                             //!< ��� �����
	IterationSimple parameter;                                     //!< ������������ ��������
	TDMAlgorithm TDMAobject;                                 //!< ����� ��������
	
	double** lastValue;                                      //!< ��������� ����������� �������� �� ����� �����
	double** tempValue;                                      //!< ������������� ����������� �������� �� ����� �����
	double** currentValue;                                   //!< ������� ����������� �������� �� ����� �����
	double** residual;                                       //!< �������� ������� �� ����� �����

	const static double epsilon;                             //!< ��������, � ������� ������ ������������ ��������� �������
public:
	IterationMethod();                                                           //!< �����������
	                                                                             //   (����� ����� - 11,
																				 //    ����� ���������� ����� - 1)
	IterationMethod(int nodesNumber_, double gridLength_, IterationSimple parameter_); //!< ����������� � �����������
	                                                                             //   (����� ����� - iteration_,
	                                                                             //    ����� ���������� ����� - gridLength_,
	                                                                             //    ������������ �������� - parameter_)
	~IterationMethod();                                                          //!< ����������
	
	double getFunctionValue(double x, double y);             //!< �������� �������� ������� f(x, y)
	double getAnalyticValue(double x, double y);             //!< �������� ������� ������ ������� � ����� (x, y)

	void leftRightTDM(double** TDM, int yNode, int iteration);
	void rightLeftTDM(double** TDM, int xNode, int iteration);

	void runOneStep(int iteration);                          //!< ��������� ���� ��� ��������
	double operatorA(double** array, int x, int y);          //!< �������� ��������� A

	bool checkResults();                                     //!< ��������� ���������� ��������
	void saveResults(std::string fileName);                  //!< ��������� ���������� ��������
};

#endif // ITERATIONMETHOD_H