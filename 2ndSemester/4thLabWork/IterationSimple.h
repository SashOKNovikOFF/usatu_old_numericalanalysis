#ifndef ITERATIONSIMPLE_H
#define ITERATIONSIMPLE_H

#include "Iteration.h"

class IterationSimple : public Iteration               // ��������
{
public:
	IterationSimple();                                 //!< �����������
												       //   (����� ���������� ����� - 1,
	                                                   //    ��� ����� - 0.1)
	IterationSimple(double gridLength_, double step_); //!< ����������� � �����������
				                                       //   (����� ���������� ����� - gridLength_,
												       //    ��� ����� - step_)
	~IterationSimple();                                //!< ����������
	
	int getCoeff_N();                          //!< �������� ���������� ��������
	virtual double getIterator_1(int iteration);       //!< �������� ������������ �������� 1
    virtual double getIterator_2(int iteration);       //!< �������� ������������ �������� 2
};

#endif // ITERATION_H