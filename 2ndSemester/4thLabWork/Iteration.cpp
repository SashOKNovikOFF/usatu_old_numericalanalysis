#define _USE_MATH_DEFINES

// Relative includes
#include "Iteration.h"

// C++ includes
#include <cmath>

const double Iteration::epsilon = 0.0001;

Iteration::Iteration()
{
	gridLength = 1.;
	step = 0.1;

	leftBorder = 4 * pow(sin(M_PI * step / 2. / gridLength), 2) / step / step;
	rightBorder = 4 * pow(cos(M_PI * step / 2. / gridLength), 2) / step / step;
	coeff_A = (rightBorder - leftBorder) / (rightBorder + leftBorder);
	coeff_B = (rightBorder + leftBorder) / (rightBorder - leftBorder) * coeff_A;
	coeff_R = rightBorder;
	coeff_S = rightBorder * (1 - coeff_B) / (1 + coeff_B);
	coeff_T = (1 - coeff_B) / (1 + coeff_B);
	coeff_ETA = (1 - coeff_A) / (1 + coeff_A);
	coeff_N = ceil(log(4 / coeff_ETA) * log(4 / epsilon) / M_PI / M_PI);
	coeff_Q = coeff_ETA * coeff_ETA * (1 + coeff_ETA * coeff_ETA / 2.) / 16.;
};
Iteration::Iteration(double gridLength_, double step_)
{
	gridLength = gridLength_;
	step = step_;

	leftBorder = 4 * pow(sin(M_PI * step / 2. / gridLength), 2) / step / step;
	rightBorder = 4 * pow(cos(M_PI * step / 2. / gridLength), 2) / step / step;
	coeff_A = (rightBorder - leftBorder) / (rightBorder + leftBorder);
	coeff_B = (rightBorder + leftBorder) / (rightBorder - leftBorder) * coeff_A;
	coeff_R = rightBorder;
	coeff_S = rightBorder * (1 - coeff_B) / (1 + coeff_B);
	coeff_T = (1 - coeff_B) / (1 + coeff_B);
	coeff_ETA = (1 - coeff_A) / (1 + coeff_A);
	coeff_N = ceil(log(4 / coeff_ETA) * log(4 / epsilon) / M_PI / M_PI);
	coeff_Q = coeff_ETA * coeff_ETA * (1 + coeff_ETA * coeff_ETA / 2.) / 16.;
};
Iteration::~Iteration()
{

};

double Iteration::getCoeff_X(int iteration)
{
	double coeffSIGMA = (2 * iteration - 1) / 2. / iteration;
	double tempNumer = (1 + pow(coeff_Q, 1 - coeffSIGMA) + pow(coeff_Q, 1 + coeffSIGMA));
	double tempDenom = (1 + pow(coeff_Q, coeffSIGMA) + pow(coeff_Q, 2 - coeffSIGMA));

	return sqrt(coeff_ETA) * pow(coeff_Q, (2 * coeffSIGMA - 1) / 4.) * tempNumer / tempDenom;
};

int Iteration::getCoeff_N()
{
	return coeff_N;
};
double Iteration::getIterator_1(int iteration)
{
	return (coeff_R * getCoeff_X(iteration) + coeff_S) / (1 + coeff_T * getCoeff_X(iteration));
};
double Iteration::getIterator_2(int iteration)
{
	return (coeff_R * getCoeff_X(iteration) - coeff_S) / (1 - coeff_T * getCoeff_X(iteration));
};