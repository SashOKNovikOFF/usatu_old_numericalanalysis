#ifndef TDMALGORITHM_H
#define TDMALGORITHM_H

// User includes
//#include "InputOutput.h"

class TDMAlgorithm //!< Реализация метода прогонки
{
public:
	TDMAlgorithm();                                                //!< Конструктор класса
    void getSolution(double** matrix, double* solution, int size); //!< Метод прогонки
};

#endif // TDMALGORITHM_H
