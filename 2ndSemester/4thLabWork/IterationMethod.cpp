#define _USE_MATH_DEFINES

// Relative includes
#include "IterationMethod.h"

// C++ includes
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>

#include <vector>

const double IterationMethod::epsilon = 0.0001;

IterationMethod::IterationMethod()
{
	nodesNumber = 11;
	gridLength = 1.;
	step = gridLength / (nodesNumber - 1);
	
	lastValue    = new double* [nodesNumber];
	tempValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	residual     = new double* [nodesNumber];
	for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		tempValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
		residual[node]     = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0.;
			tempValue[x][y] = 0.;
			currentValue[x][y] = 0.;
			residual[x][y] = 0.;
		};
};
IterationMethod::IterationMethod(int nodesNumber_, double gridLength_, IterationSimple parameter_)
{
	nodesNumber = nodesNumber_;
	gridLength = gridLength_;
	step = gridLength / (nodesNumber_ - 1);
	parameter = parameter_;
	
	lastValue    = new double* [nodesNumber];
	tempValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	residual     = new double* [nodesNumber];
	for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		tempValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
		residual[node]     = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0.;
			tempValue[x][y] = 0.;
			currentValue[x][y] = 0.;
			residual[x][y] = 0.;
		};
};
IterationMethod::~IterationMethod()
{

};

double IterationMethod::getFunctionValue(double x, double y)
{
	return 2. * (x + y - x * x - y * y);
}
double IterationMethod::getAnalyticValue(double x, double y)
{
	return (x - x * x) * (y - y * y);
};

void IterationMethod::leftRightTDM(double** TDM, int yNode, int iteration)
{
	TDM[0][0] = 0.;
	TDM[1][0] = 1.;
	TDM[2][0] = 0.;
	TDM[3][0] = 0.;

	TDM[0][nodesNumber - 1] = 0.;
	TDM[1][nodesNumber - 1] = 1.;
	TDM[2][nodesNumber - 1] = 0.;
	TDM[3][nodesNumber - 1] = 0.;

	for (int i = 1; i < nodesNumber - 1; i++)
	{
		TDM[0][i] = - 1.;
		TDM[1][i] = 2 + step * step * parameter.getIterator_1(iteration);
		TDM[2][i] = - 1.;
		TDM[3][i] = lastValue[i][yNode - 1] - (2 - step * step * parameter.getIterator_1(iteration)) * lastValue[i][yNode] + lastValue[i][yNode + 1]  + step * step * getFunctionValue(i * step, yNode * step);
	};

	double* solution = new double [nodesNumber];
	TDMAobject.getSolution(TDM, solution, nodesNumber);
	for (int i = 0; i < nodesNumber; i++)
		tempValue[i][yNode] = solution[i];

	delete solution;
};
void IterationMethod::rightLeftTDM(double** TDM, int xNode, int iteration)
{
	TDM[0][0] = 0.;
	TDM[1][0] = 1.;
	TDM[2][0] = 0.;
	TDM[3][0] = 0.;

	TDM[0][nodesNumber - 1] = 0.;
	TDM[1][nodesNumber - 1] = 1.;
	TDM[2][nodesNumber - 1] = 0.;
	TDM[3][nodesNumber - 1] = 0.;

	for (int j = 1; j < nodesNumber - 1; j++)
	{
		TDM[0][j] = -1.;
		TDM[1][j] = 2 + step * step * parameter.getIterator_2(iteration);
		TDM[2][j] = -1.;
		TDM[3][j] = tempValue[xNode - 1][j] - (2 - step * step * parameter.getIterator_2(iteration)) * tempValue[xNode][j] + tempValue[xNode + 1][j] + step * step * getFunctionValue(xNode * step, j * step);
	};

	double* solution = new double[nodesNumber];
	TDMAobject.getSolution(TDM, solution, nodesNumber);
	for (int j = 0; j < nodesNumber; j++)
		currentValue[xNode][j] = solution[j];

	delete solution;
};
void IterationMethod::runOneStep(int iteration)
{
	double** TDM = new double* [4];
	for (int node = 0; node < 4; node++)
		TDM[node] = new double [nodesNumber];

	for (int node = 1; node < nodesNumber - 1; node++)
		leftRightTDM(TDM, node, iteration);
    for (int node = 1; node < nodesNumber - 1; node++)
		rightLeftTDM(TDM, node, iteration);

	for (int x = 1; x < nodesNumber - 1; x++)
		for (int y = 1; y < nodesNumber - 1; y++)
			residual[x][y] = operatorA(currentValue, x, y) - getFunctionValue(x * step, y * step);

	double** temp;
	temp = lastValue;
	lastValue = currentValue;
	currentValue = temp;

	delete TDM;
};
bool IterationMethod::checkResults()
{
	double maxValue = std::abs(currentValue[1][1] - lastValue[1][1]);
	for (int x = 1; x < nodesNumber - 1; x++)
		for (int y = 1; y < nodesNumber - 1; y++)
			if (maxValue < std::abs(currentValue[x][y] - lastValue[x][y]))
				maxValue = std::abs(currentValue[x][y] - lastValue[x][y]);

	if (maxValue < epsilon)
		return true;
	else
		return false;
};
double IterationMethod::operatorA(double** array, int x, int y)
{
	return (4 * array[x][y] - array[x - 1][y] - array[x + 1][y] - array[x][y - 1] - array[x][y + 1]) / step / step;
};

void IterationMethod::saveResults(std::string fileName)
{
	std::fstream LIFile(fileName + "_LastIteration.txt", std::ifstream::out);
	std::fstream CIFile(fileName + "_CurrentIteration.txt", std::ifstream::out);
	std::fstream ASFile(fileName + "_AnalyticSolution.txt", std::ifstream::out);
    std::fstream ErrorFile(fileName + "_MeasureErrors.txt", std::ifstream::out);
	std::fstream ResidualFile(fileName + "_Residual.txt", std::ifstream::out);
	
	LIFile << "Results of computation:" << std::endl;
	CIFile << "Results of computation:" << std::endl;
	ASFile << "Results of computation:" << std::endl;
    ErrorFile << "Results of computation:" << std::endl;
	ResidualFile << "Results of computation:" << std::endl;

	for (int x = nodesNumber - 1; x >= 0; x--)
	{
		for (int y = 0; y < nodesNumber; y++)
		{
			LIFile << lastValue[x][y] << "\t";
			CIFile << currentValue[x][y] << "\t";
			ASFile << getAnalyticValue(x * step, y * step) << "\t";
            ErrorFile << currentValue[x][y] - getAnalyticValue(x * step, y * step) << "\t";
			ResidualFile << residual[x][y] << "\t";
		};
		LIFile << std::endl;
		CIFile << std::endl;
		ASFile << std::endl;
        ErrorFile << std::endl;
		ResidualFile << std::endl;
	};
};