// User includes
#include "IterationDirichlet.h"
#include "IterationMethod.h"

int main()
{
	const int nodesNumber = 11;
	const double step = 1.0 / (nodesNumber - 1);
	const int iterationNumber = 10;

	IterationDirichlet parameter(iterationNumber, step);
	IterationMethod iterationObject(nodesNumber, parameter);
	
	bool flag = false;
	while (!flag)
	{
		for (int iteration = 1; iteration <= iterationNumber; iteration++)
		{
			iterationObject.runOneStep(iteration);
			flag = iterationObject.checkResults();
		};
	};
	
	iterationObject.saveResults("1st");
	iterationObject.saveResidual("1st");

	return 0;
};