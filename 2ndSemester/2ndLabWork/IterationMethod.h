#ifndef ITERATIONMETHOD_H
#define ITERATIONMETHOD_H

// Relative includes
#include "Iteration.h"

// C++ includes
#include <string>

class IterationMethod                                        // ����������� ����������� �����
{
private:
	int nodesNumber;                                         //!< ����� �����
	double step;                                             //!< ��� �����
	Iteration parameter;                                     //!< ������������ ��������
	
	double** lastValue;                                      //!< ��������� ����������� �������� �� ����� �����
	double** currentValue;                                   //!< ������� ����������� �������� �� ����� �����

	const static double epsilon;                             //!< ��������, � ������� ������ ������������ ��������� �������
public:
	IterationMethod();                                       //!< ����������� (����� ����� - 11, ������������ �������� - ����������� ����� ���������� ��� ������ �������)
	IterationMethod(int nodesNumber_, Iteration parameter_); //!< ����������� � ����������� (����� ����� - iteration_, ������������ �������� - parameter_)
	~IterationMethod();                                      //!< ����������
	
	double getFunctionValue(double x, double y);             //!< �������� �������� ������� f(x, y)
	double getAnalyticValue(double x, double y);             //!< �������� ������� ������ ������� � ����� (x, y)
	void runOneStep(int iteration);                          //!< ��������� ���� ��� ��������
	bool checkResults();                                     //!< ��������� ���������� ��������
	void saveResults(std::string fileName);                  //!< ��������� ���������� ��������
	void saveResidual(std::string fileName);                 //!< ��������� �������� ������� �� �����
};

#endif // ITERATIONMETHOD_H