#define _USE_MATH_DEFINES

// Relative includes
#include "Iteration.h"

// C++ includes
#include <cmath>

double Iteration::getChebyshevRoot(int iteration)
{
	return cos(M_PI * (2. * iteration - 1.) / 2. / iterationNumber);
};
double Iteration::getInitialParameter()
{
	return 2. / (getLambdaMin() + getLambdaMax());
};
double Iteration::getRoCoefficient()
{
	return (getLambdaMax() - getLambdaMin()) / (getLambdaMax() + getLambdaMin());
};
double Iteration::getLambdaMin()
{
	return 8. * pow(sin(step * M_PI / 2.), 2.) / (step * step);
};
double Iteration::getLambdaMax()
{
	return 8 * pow(cos(step * M_PI / 2.), 2.) / (step * step);
};

Iteration::Iteration()
{
	iterationNumber = 10;
	step = 0.1;
};
Iteration::Iteration(int iterationNumber_, double step_)
{
	iterationNumber = iterationNumber_;
	step = step_;
};
Iteration::~Iteration()
{

};

double Iteration::getParameter(int iteration)
{
	return getInitialParameter() / (1.0 + getRoCoefficient() * getChebyshevRoot(iteration));
};