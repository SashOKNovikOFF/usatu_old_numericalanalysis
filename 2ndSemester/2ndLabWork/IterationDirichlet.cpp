#define _USE_MATH_DEFINES

// Relative includes
#include "IterationDirichlet.h"

// C++ includes
#include <cmath>

IterationDirichlet::IterationDirichlet() : Iteration()
{

};
IterationDirichlet::IterationDirichlet(int iterationNumber_, double step_) : Iteration(iterationNumber_, step_)
{

};
IterationDirichlet::~IterationDirichlet()
{

};

double IterationDirichlet::getInitialParameter()
{
	return step * step / 4.0;
};
double IterationDirichlet::getRoCoefficient()
{
	return cos(step * M_PI);
	//return 1. - tan(M_PI*step / 2)*tan(M_PI*step / 2);
};