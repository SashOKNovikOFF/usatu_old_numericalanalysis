#ifndef ITERATION_SIMPLE_H
#define ITERATION_SIMPLE_H

// Relative includes
#include "Iteration.h"

class IterationDirichlet : public Iteration                 // �������� ��� ������ �������
{
protected:
	virtual double getInitialParameter();                   //!< �������� ��������� ������������ ��������
	virtual double getRoCoefficient();                      //!< �������� ����������� Ro, ������������ ��� ������� ���������
public:
	IterationDirichlet();                                   //!< ����������� (����� �������� - 10, ��� ����� - 0,1)
	IterationDirichlet(int iterationNumber_, double step_); //!< ����������� � ����������� (����� �������� - iterationNumber_, ��� ����� - step_)
	~IterationDirichlet();                                  //!< ����������
};

#endif // ITERATION_SIMPLE_H