#ifndef ITERATION_H
#define ITERATION_H

class Iteration                                    // ��������
{
protected:
	int iterationNumber;                           //!< ����� ��������
	double step;                                   //!< ��� �����

	double getChebyshevRoot(int iteration);        //!< �������� ������ �������� ��������
	virtual double getInitialParameter();          //!< �������� ��������� ������������ ��������
	virtual double getRoCoefficient();             //!< �������� ����������� Ro, ������������ ��� ������� ���������
	double getLambdaMin();                         //!< �������� ����������� ����������� ��������
	double getLambdaMax();                         //!< �������� ������������ ����������� ��������
public:
	Iteration();                                   //!< ����������� (����� �������� - 10, ��� ����� - 0.1)
	Iteration(int iterationNumber_, double step_); //!< ����������� � ����������� (����� �������� - iterationNumber_, ��� ����� - step_)
	~Iteration();                                  //!< ����������
	 
	double getParameter(int iteration);            //!< �������� ������������ ��������
};

#endif // ITERATION_H