#define _USE_MATH_DEFINES

// Relative includes
#include "IterationMethod.h"

// C++ includes
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>

const double IterationMethod::epsilon = 0.0001;

IterationMethod::IterationMethod()
{
	nodesNumber = 11;
	step = 1.0 / (nodesNumber - 1);
	
	lastValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0;
			currentValue[x][y] = 0;
		};
};
IterationMethod::IterationMethod(int nodesNumber_, Iteration parameter_)
{
	nodesNumber = nodesNumber_;
	step = 1.0 / (nodesNumber_ - 1);
	parameter = parameter_;
	
	lastValue    = new double* [nodesNumber];
	currentValue = new double* [nodesNumber];
	for (int node = 0; node < nodesNumber; node++)
	{
		lastValue[node]    = new double [nodesNumber];
		currentValue[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
			lastValue[x][y] = 0.;
			currentValue[x][y] = 0.;
		};
};
IterationMethod::~IterationMethod()
{

};

double IterationMethod::getFunctionValue(double x, double y)
{
	return 2. * (x + y - x * x - y * y);
}
double IterationMethod::getAnalyticValue(double x, double y)
{
	return (x - x * x) * (y - y * y);
};
void IterationMethod::runOneStep(int iteration)
{
	for (int x = 1; x < nodesNumber - 1; x++)
		for (int y = 1; y < nodesNumber - 1; y++)
		{
			double differential[2];
			differential[0] = (lastValue[x - 1][y] - 2 * lastValue[x][y] + lastValue[x + 1][y]) / (step * step);
			differential[1] = (lastValue[x][y - 1] - 2 * lastValue[x][y] + lastValue[x][y + 1]) / (step * step);
			
			double AY = - (differential[0] + differential[1]);
			currentValue[x][y] = lastValue[x][y] + parameter.getParameter(iteration) * (getFunctionValue(x * step, y * step) - AY);
		};

	double** temp;
	temp         = lastValue;
	lastValue    = currentValue;
	currentValue = temp;
};
bool IterationMethod::checkResults()
{
	double maxValue = std::abs(currentValue[1][1] - lastValue[1][1]);
	for (int x = 1; x < nodesNumber - 1; x++)
		for (int y = 1; y < nodesNumber - 1; y++)
			if (maxValue < std::abs(currentValue[x][y] - lastValue[x][y]))
				maxValue = std::abs(currentValue[x][y] - lastValue[x][y]);
	
	if (maxValue < epsilon)
		return true;
	else
		return false;
};
void IterationMethod::saveResults(std::string fileName)
{
	std::fstream LIFile(fileName + "_LastIteration.txt", std::ifstream::out);
	std::fstream CIFile(fileName + "_CurrentIteration.txt", std::ifstream::out);
	std::fstream ASFile(fileName + "_AnalyticSolution.txt", std::ifstream::out);
    std::fstream ErrorFile(fileName + "_MeasureErrors.txt", std::ifstream::out);
	
	LIFile << "Results of computation:" << std::endl;
	CIFile << "Results of computation:" << std::endl;
	ASFile << "Results of computation:" << std::endl;
    ErrorFile << "Results of computation:" << std::endl;

	for (int x = nodesNumber - 1; x >= 0; x--)
	{
		for (int y = 0; y < nodesNumber; y++)
		{
			LIFile << lastValue[x][y] << "\t";
			CIFile << currentValue[x][y] << "\t";
			ASFile << getAnalyticValue(x * step, y * step) << "\t";
            ErrorFile << currentValue[x][y] - getAnalyticValue(x * step, y * step) << "\t";
		};
		LIFile << std::endl;
		CIFile << std::endl;
		ASFile << std::endl;
        ErrorFile << std::endl;
	};
}
void IterationMethod::saveResidual(std::string fileName)
{
	std::fstream ResidualFile(fileName + "_Residal.txt", std::ifstream::out);

	ResidualFile << "Residal (without border values):" << std::endl;

	for (int x = nodesNumber - 2; x >= 1; x--)
	{
		for (int y = 1; y < nodesNumber - 1; y++)
		{
			double differential[2];
			differential[0] = (currentValue[x - 1][y] - 2 * currentValue[x][y] + currentValue[x + 1][y]) / (step * step);
			differential[1] = (currentValue[x][y - 1] - 2 * currentValue[x][y] + currentValue[x][y + 1]) / (step * step);
			double AY = -(differential[0] + differential[1]);

			differential[0] = (getAnalyticValue(x - 1, y) - 2 * getAnalyticValue(x, y) + getAnalyticValue(x + 1, y)) / (step * step);
			differential[1] = (getAnalyticValue(x, y - 1) - 2 * getAnalyticValue(x, y) + getAnalyticValue(x, y + 1)) / (step * step);
			double AU = -(differential[0] + differential[1]);

			ResidualFile << AY - getFunctionValue(x * step, y * step) << "\t";
		};
		ResidualFile << std::endl;
	};

    double temp = 0.;
    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            temp += pow(getFunctionValue(x, y), 2.);
    temp = sqrt(temp);

    ResidualFile << "sqrt(SUM f(x, y)) - " << temp << std::endl;
};