// User includes
#include "IterationMethod.h"

int main()
{
	const int nodesNumber = 11;
	const int iterationNumber = 5;
    const double dt = 0.1;
    const int step = 5;

	IterationMethod iterationSI(nodesNumber);
	IterationMethod iterationCH(nodesNumber);
	IterationMethod iterationDW(nodesNumber);
	
    for (double i = 1; i <= step; i++)
    {
        bool flag = false;
        iterationSI.resaveData();
        while (!flag)
        {
            iterationSI.runSIStep(dt);
            flag = iterationSI.checkResults();
        };

        iterationSI.changeTime(dt);
    };
    
	iterationSI.saveResults("SimpleIteration");
    
    for (double i = 1; i <= step; i++)
    {
        bool flag = false;
        iterationCH.resaveData();
        while (!flag)
        {
            for (int i = 0; i < iterationNumber; i++)
                iterationCH.runCHStep(dt, i, iterationNumber);
            flag = iterationCH.checkResults();
        };

        iterationCH.changeTime(dt);
    };
    
	iterationCH.saveResults("Chebyshev");
    
    for (double i = 1; i <= step; i++)
    {
        bool flag = false;
        iterationDW.resaveData();
        while (!flag)
        {
            iterationDW.runDWStep(dt);
            flag = iterationDW.checkResults();
        };

        iterationDW.changeTime(dt);
    };

	iterationDW.saveResults("FastDown");

	return 0;
};