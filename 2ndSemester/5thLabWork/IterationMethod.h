#ifndef ITERATIONMETHOD_H
#define ITERATIONMETHOD_H

// C++ includes
#include <string>

class IterationMethod                                        // ����� ������-��������
{
private:
	int nodesNumber;                                         //!< ����� �����
	double step;                                             //!< ��� �����
    double iterationParameter;                               //!< ������������ ��������
	double time;                                             //!< ����� ��������
	
    double** previousValue;                                  //!< �������� �� ����� ����� �� (k)-�� ��������� ����
	double** lastValue;                                      //!< �������� �� ����� ����� �� (k + 1)-�� ��������� ���� �� (n)-�� ��������
	double** currentValue;                                   //!< �������� �� ����� ����� �� (k + 1)-�� ��������� ���� �� (n + 1)-�� ��������
    double** residual;                                       //!< �������� ������� �� �����
    double** operatorResidual;                               //!< �������� ����������� ���������, ���������������� �� �������, �� �����
    
    double operatorA(double** array, int x, int y);             //!< ��������� �������� ��������� A1 � ���� �����
    double operatorF(double** array, int x, int y, double dt);  //!< ��������� �������� ��������� F � ���� �����
	double operatorA1(double** array, int x, int y, double dt); //!< ��������� �������� ��������� A � ���� �����

	double getScalarProduct(double** x, double** y);         //!< ��������� ��������� ������������

	const static double epsilon;                             //!< ��������, � ������� ������ ������������ ��������� �������
public:
	IterationMethod();                                       //!< ����������� (����� ����� - 11)
    IterationMethod(int nodesNumber_);                       //!< ����������� � ����������� (����� ����� - iteration_)
	~IterationMethod();                                      //!< ����������
	
	double getFunctionValue(double x, double y, double t);   //!< �������� �������� ������� f(x, y, t)
	double getAnalyticValue(double x, double y, double t);   //!< �������� ������� ��������� ���������������� � ����� (x, y, t)
    double getInitialValue(double x, double y);              //!< �������� ��������� �������� � �����(x, y)

    void resaveData();                                           //!< �������� ������ �� ��������� �� ����� ����� �� (k)-�� ��������� ����
    void runSIStep(double dt);                                   //!< ��������� ���� ��� ������� ������� ��������
    void runCHStep(double dt, int parameter_, double iteration); //!< ��������� ���� ��� ������� ��������
    void runDWStep(double dt);                                   //!< ��������� ���� ��� ������� ���������� ������
	void changeTime(double dt);                                  //!< ������� �� ��������� ��� �� �������

    void correctValue();                                     //!< ��������������� ��������� ��������

	bool checkResults();                                     //!< ��������� ���������� ��������
	void saveResults(std::string fileName);                  //!< ��������� ���������� ��������
};

#endif // ITERATIONMETHOD_H