#define _USE_MATH_DEFINES

// Relative headers
#include "IterationMethod.h"

// C++ includes
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>

#include <vector>

const double IterationMethod::epsilon = 0.0001;

IterationMethod::IterationMethod()
{
	nodesNumber = 11;
	step = 1.0 / (nodesNumber - 1);
	time = 0.0;

    previousValue = new double* [nodesNumber];
	lastValue     = new double* [nodesNumber];
	currentValue  = new double* [nodesNumber];
	
    residual = new double* [nodesNumber];
	operatorResidual = new double* [nodesNumber];
	
    for (int node = 0; node < nodesNumber; node++)
	{
        previousValue[node] = new double [nodesNumber];
		lastValue[node]     = new double [nodesNumber];
		currentValue[node]  = new double [nodesNumber];
		
        residual[node] = new double [nodesNumber];
		operatorResidual[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
            previousValue[x][y] = getInitialValue(x * step, y * step);
			lastValue[x][y] = previousValue[x][y];
			currentValue[x][y] = 0.;

            residual[x][y] = 0.;
            operatorResidual[x][y] = 0.;
		};
};
IterationMethod::IterationMethod(int nodesNumber_)
{
	nodesNumber = nodesNumber_;
	step = 1.0 / (nodesNumber_ - 1);
	time = 0.0;

    previousValue = new double* [nodesNumber];
	lastValue     = new double* [nodesNumber];
	currentValue  = new double* [nodesNumber];
	
    residual = new double* [nodesNumber];
	operatorResidual = new double* [nodesNumber];
	
    for (int node = 0; node < nodesNumber; node++)
	{
        previousValue[node] = new double [nodesNumber];
		lastValue[node]     = new double [nodesNumber];
		currentValue[node]  = new double [nodesNumber];
		
        residual[node] = new double [nodesNumber];
		operatorResidual[node] = new double [nodesNumber];
	};
	
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
		{
            previousValue[x][y] = getInitialValue(x * step, y * step);
            lastValue[x][y] = previousValue[x][y];
			currentValue[x][y] = 0.;

            residual[x][y] = 0.;
            operatorResidual[x][y] = 0.;
		};
};
IterationMethod::~IterationMethod()
{

};

double IterationMethod::operatorA(double** array, int x, int y)
{
	return (4 * array[x][y] - array[x - 1][y] - array[x + 1][y] - array[x][y - 1] - array[x][y + 1]) / step / step;
};

double IterationMethod::operatorF(double** array, int x, int y, double dt)
{
	return array[x][y] - dt / 2. * operatorA(array, x, y) + dt * getFunctionValue(x * step, y * step, time + dt / 2.);
};

double IterationMethod::operatorA1(double** array, int x, int y, double dt)
{
	return array[x][y] + dt / 2. * operatorA(array, x, y);
};

double IterationMethod::getScalarProduct(double** x, double** y)
{
	double tempSum = 0.;
	for (int i = 0; i < nodesNumber; i++)
		for (int j = 0; j < nodesNumber; j++)
			tempSum += x[i][j] * y[i][j]; //(x[i][j] - y[i][j]) * (x[i][j] - y[i][j]);
	//tempSum = sqrt(tempSum);

	return tempSum;
};

double IterationMethod::getFunctionValue(double x, double y, double t)
{
    return exp(2.2 * t + x) * (2 * (x + y - x * x - y * y) + 1.2 * (x - x * x) * (y - y * y) - 2 * (1 - 2 * x) * (y - y * y));
};
double IterationMethod::getAnalyticValue(double x, double y, double t)
{
    return (x - x * x) * (y - y * y) * exp(2.2 * t + x);
};
double IterationMethod::getInitialValue(double x, double y)
{
    return (x - x * x) * (y - y * y) * exp(x);
};

void IterationMethod::resaveData()
{
    for (int x = 1; x < nodesNumber - 1; x++)
        for (int y = 1; y < nodesNumber - 1; y++)
            previousValue[x][y] = lastValue[x][y];
};
void IterationMethod::runSIStep(double dt)
{
    double parameter = 2. / (2 + 9 * dt + 4 * dt / step / step);
    for (int x = 1; x < nodesNumber - 1; x++)
        for (int y = 1; y < nodesNumber - 1; y++)
            currentValue[x][y] = lastValue[x][y] - parameter * operatorA1(lastValue, x, y, dt) + parameter * operatorF(previousValue, x, y, dt);

    double** temp;
    temp = lastValue;
    lastValue = currentValue;
    currentValue = temp;

    for (int x = 1; x < nodesNumber - 1; x++)
        for (int y = 1; y < nodesNumber - 1; y++)
            residual[x][y] = operatorA1(currentValue, x, y, dt) - operatorF(lastValue, x, y, dt);
};
void IterationMethod::runCHStep(double dt, int parameter_, double iteration)
{
    double gammaLeft = 1 + 9 * dt;
    double gammaRight = 1 + 4 * dt / step / step;
    double ro = (gammaRight - gammaLeft) / (gammaLeft - gammaRight);
    double T0 = 2. / (gammaLeft + gammaRight);
    double parameter = T0 / (1 + ro * cos((2 * parameter_ + 1) * M_PI / 2. / iteration));

    for (int x = 1; x < nodesNumber - 1; x++)
        for (int y = 1; y < nodesNumber - 1; y++)
            currentValue[x][y] = lastValue[x][y] - parameter * operatorA1(lastValue, x, y, dt) + parameter * operatorF(previousValue, x, y, dt);

    double** temp;
    temp = lastValue;
    lastValue = currentValue;
    currentValue = temp;

    for (int x = 1; x < nodesNumber - 1; x++)
        for (int y = 1; y < nodesNumber - 1; y++)
            residual[x][y] = operatorA1(currentValue, x, y, dt) - operatorF(lastValue, x, y, dt);
};
void IterationMethod::runDWStep(double dt)
{
     for (int x = 1; x < nodesNumber - 1; x++)
         for (int y = 1; y < nodesNumber - 1; y++)
            residual[x][y] = operatorA1(lastValue, x, y, dt) - operatorF(previousValue, x, y, dt);

     for (int x = 1; x < nodesNumber - 1; x++)
         for (int y = 1; y < nodesNumber - 1; y++)
           operatorResidual[x][y] = operatorA1(residual, x, y, dt);

     iterationParameter = getScalarProduct(residual, residual) / getScalarProduct(operatorResidual, residual);

     for (int x = 1; x < nodesNumber - 1; x++)
         for (int y = 1; y < nodesNumber - 1; y++)
           currentValue[x][y] = lastValue[x][y] - iterationParameter * residual[x][y];

     double** temp;
     temp         = lastValue;
     lastValue    = currentValue;
     currentValue = temp;
     
     //for (int x = 1; x < nodesNumber - 1; x++)
     //    for (int y = 1; y < nodesNumber - 1; y++)
     //        residual[x][y] = operatorA1(currentValue, x, y, dt) - operatorF(lastValue, x, y, time);
};
void IterationMethod::changeTime(double dt)
{
	time += dt;
};

void IterationMethod::correctValue()
{
    double tempSum = 0.;
    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            tempSum += currentValue[x][y];

    tempSum /= (nodesNumber + 1) * (nodesNumber + 1);

    for (int x = 0; x < nodesNumber; x++)
        for (int y = 0; y < nodesNumber; y++)
            currentValue[x][y] -= tempSum;
};

bool IterationMethod::checkResults()
{
	double maxValue = std::abs(currentValue[0][0] - lastValue[0][0]);
	for (int x = 0; x < nodesNumber; x++)
		for (int y = 0; y < nodesNumber; y++)
			if (maxValue < std::abs(currentValue[x][y] - lastValue[x][y]))
				maxValue = std::abs(currentValue[x][y] - lastValue[x][y]);
	
	if (maxValue < epsilon)
		return true;
	else
		return false;
};
void IterationMethod::saveResults(std::string fileName)
{
	std::fstream LIFile(fileName + "_LastIteration.txt", std::ifstream::out);
	std::fstream CIFile(fileName + "_CurrentIteration.txt", std::ifstream::out);
	std::fstream ASFile(fileName + "_AnalyticSolution.txt", std::ifstream::out);
    std::fstream ErrorFile(fileName + "_MeasureErrors.txt", std::ifstream::out);
    std::fstream ResidualFile(fileName + "_Residal.txt", std::ifstream::out);
	
	LIFile << "Results of computation:" << std::endl;
	CIFile << "Results of computation:" << std::endl;
	ASFile << "Results of computation:" << std::endl;
    ErrorFile << "Results of computation:" << std::endl;
    ResidualFile << "Results of computation:" << std::endl;

	for (int x = nodesNumber - 1; x >= 0; x--)
	{
		for (int y = 0; y < nodesNumber; y++)
		{
			LIFile << lastValue[x][y] << "\t";
			CIFile << currentValue[x][y] << "\t";
            ASFile << getAnalyticValue(x * step, y * step, time) << "\t";
            ErrorFile << currentValue[x][y] - getAnalyticValue(x * step, y * step, time) << "\t";
            ResidualFile << residual[x][y] << "\t";
		};
		LIFile << std::endl;
		CIFile << std::endl;
		ASFile << std::endl;
        ErrorFile << std::endl;
        ResidualFile << std::endl;
	};
};