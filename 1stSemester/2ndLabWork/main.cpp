// C++ standard includes
#include <iostream>
#include <ios>

// User includes
#include "Dichotomy.h"
#include "IterativeMethod.h"
#include "NewtonMethod.h"

int main()
{
    try
    {
        int k = 1;
        int m = 3;
        int l = 2;

        double x;
        double initialX;

        double epsilon = 1E-7;
        std::cout.precision(10);

        Dichotomy dichotomyObject(k, m, l);
        x = dichotomyObject.getSolution(epsilon);

        std::cout << "Dichotomy solution [x, y]: ";
        std::cout << "[" << x << ", " << dichotomyObject.getFunctionValue(x) << "]" << std::endl;

        IterativeMethod iterativeObject(k, m, l);
        initialX = dichotomyObject.getSolution(1E-1);
        x = iterativeObject.getSolution(initialX, epsilon);

        std::cout << "Iterative solution [x, y]: ";
        std::cout << "[" << x << ", " << iterativeObject.getFunctionValue(x) << "]" << std::endl;

        NewtonMethod NewtonObject(k, m, l);
        initialX = dichotomyObject.getSolution(1E-1);
        x = NewtonObject.getSolution(initialX, epsilon);

        std::cout << "Newton's solution [x, y]: ";
        std::cout << "[" << x << ", " << NewtonObject.getFunctionValue(x) << "]" << std::endl;

        std::cout << std::endl;

        vector<double> v;
        vector<double> initialV = { 0.5, 0.5 };
        v = iterativeObject.getSystemSolution(initialV, epsilon);

        std::cout << "System iteration solution [x, y]: ";
        std::cout << "[" << v[0] << ", " << v[1] << "]" << std::endl;

        v = NewtonObject.getSystemSolution(initialV, epsilon);

        std::cout << "System Newton's solution [x, y]: ";
        std::cout << "[" << v[0] << ", " << v[1] << "]" << std::endl;

        return 0;
    }
    catch(char const* error)
    {
        std::cout << error << std::endl;
        return 1;
    };
};
