#ifndef DICHOTOMY_H
#define DICHOTOMY_H

class Dichotomy //!< Реализация дихотомии
{
private:
    int k, m, l;                        //!< Коэффициенты для заданной функции
public:
    Dichotomy();                        //!< Конструктор класса
    Dichotomy(int k, int m, int l);     //!< Конструктор с параметрами

    double getSolution(double epsilon); //!< Решение уравнения методом дихотомии
    double getFunctionValue(double x);  //!< Значение функции
};

#endif // DICHOTOMY_H