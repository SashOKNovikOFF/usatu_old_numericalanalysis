#ifndef ITERATIVEMETHOD_H
#define ITERATIVEMETHOD_H

// C++ standard includes
#include <vector>
using std::vector;

class IterativeMethod //!< Реализация метода простых итераций
{
private:
    //!< Коэффициенты для заданной функции
    int k, m, l;

    //!< Коэффициенты для заданной системы уравнений
    const double a = 0.24;
    const double b = 3.5;
    const double c = 2.0;
public:
    IterativeMethod();                                                        //!< Конструктор класса
    IterativeMethod(int k, int m, int l);                                     //!< Конструктор с параметрами

    double getSolution(double initialX, double epsilon);                      //!< Решение уравнения методом простых итераций
    double getFunctionValue(double x);                                        //!< Значение функции
    double getPhiValue(double x);                                             //!< Значение функции phi(x)

    vector<double> getSystemSolution(vector<double> initialV, double epsilon); //!< Решение системы уравнений методом простых итераций
    vector<double> getSystemFunctionValue(vector<double> iteration);           //!< Значения функций системы уравнений
    vector<double> getSystemPhiValue(vector<double> iteration);                //!< Значения функций phi(x) системы уравнений
};

#endif // ITERATIVEMETHOD_H