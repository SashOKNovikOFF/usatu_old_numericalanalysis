// Related headers
#include "IterativeMethod.h"

// C++ standard includes
#include <cmath>
#include <iostream>

const double PI = 3.14;

IterativeMethod::IterativeMethod()
{
    k = 1;
    m = 1;
    l = 1;
};

IterativeMethod::IterativeMethod(int k, int m, int l)
{
    this->k = k;
    this->m = m;
    this->l = l;
};

double IterativeMethod::getSolution(double initialX, double epsilon)
{
    double firstX  = initialX;
    double secondX = getPhiValue(firstX);
    double thirdX  = getPhiValue(secondX);

    double convergence = pow(thirdX - secondX, 2) / std::abs(2 * secondX - thirdX - firstX);

    while (convergence >= epsilon)
    {
        firstX  = secondX;
        secondX = thirdX;
        thirdX  = getPhiValue(secondX);

        convergence = pow(thirdX - secondX, 2) / std::abs(2 * secondX - thirdX - firstX);
    }

    return thirdX;
};

double IterativeMethod::getFunctionValue(double x)
{
    return k * cos(PI * pow(x, m)) + pow(x, l) * (1 - x);
};

double IterativeMethod::getPhiValue(double x)
{
    return pow(acos((x - 1) * pow(x, l) / k) / PI, 1.0 / m);
};

vector<double> IterativeMethod::getSystemSolution(vector<double> initialV, double epsilon)
{
    vector<double> firstV  = initialV;
    vector<double> secondV = getSystemPhiValue(firstV);
    vector<double> thirdV  = getSystemPhiValue(secondV);

    double convergenceX = pow(thirdV[0] - secondV[0], 2) / std::abs(2 * secondV[0] - thirdV[0] - firstV[0]);
    double convergenceY = pow(thirdV[1] - secondV[1], 2) / std::abs(2 * secondV[1] - thirdV[1] - firstV[1]);

    while (!((convergenceX < epsilon) && (convergenceY < epsilon)))
    {
        firstV  = secondV;
        secondV = thirdV;
        thirdV  = getSystemPhiValue(secondV);

        convergenceX = pow(thirdV[0] - secondV[0], 2) / std::abs(2 * secondV[0] - thirdV[0] - firstV[0]);
        convergenceY = pow(thirdV[1] - secondV[1], 2) / std::abs(2 * secondV[1] - thirdV[1] - firstV[1]);
    };

    return thirdV;
};

vector<double> IterativeMethod::getSystemFunctionValue(vector<double> iteration)
{
    if (iteration.size() != 2)
        throw "Error: arguments vector size is not correct.";

    vector<double> value(2, 0);
    value[0] = cos(iteration[1]) + c * iteration[0];
    value[1] = a * iteration[0] + b * iteration[1] + pow(iteration[0], 2) * iteration[1];

    return value;
};

vector<double> IterativeMethod::getSystemPhiValue(vector<double> iteration)
{
    if (iteration.size() != 2)
        throw "Error: arguments vector size is not correct.";

    vector<double> value(2, 0);
    value[0] = - cos(iteration[1]) / c;
    value[1] = - (a * iteration[0] + pow(iteration[0], 2) * iteration[1]) / b;


    return value;
};