// Related headers
#include "NewtonMethod.h"

// C++ standard includes
#include <cmath>
#include <algorithm>
#include <functional>

// User includes
#include "GaussAlgorithm.h"

const double PI = 3.14;

NewtonMethod::NewtonMethod()
{
    k = 1;
    m = 1;
    l = 1;
};

NewtonMethod::NewtonMethod(int k, int m, int l)
{
    this->k = k;
    this->m = m;
    this->l = l;
};

double NewtonMethod::getSolution(double initialX, double epsilon)
{
    double firstX  = initialX;
    double secondX = firstX  - getFunctionValue(firstX)  / getDerivativeValue(firstX);
    double thirdX  = secondX - getFunctionValue(secondX) / getDerivativeValue(secondX);

    double convergence = pow(thirdX - secondX, 2) / std::abs(2 * secondX - thirdX - firstX);

    while (convergence >= epsilon)
    {
        firstX = secondX;
        secondX = thirdX;
        thirdX = secondX - getFunctionValue(secondX) / getDerivativeValue(secondX);

        convergence = pow(thirdX - secondX, 2) / std::abs(2 * secondX - thirdX - firstX);
    }

    return thirdX;
};

double NewtonMethod::getFunctionValue(double x)
{
    return k * cos(PI * pow(x, m)) + pow(x, l) * (1 - x);
};

double NewtonMethod::getDerivativeValue(double x)
{
    return -PI * m * k * pow(x, m  - 1) * sin(PI * pow(x, m)) + l * pow(x, l - 1) * (1 - x) - pow(x, l);
};

vector<double> NewtonMethod::getSystemSolution(vector<double> initialV, double epsilon)
{
    GaussAlgorithm GaussObject;
    vector<double> firstV  = initialV;
    vector<double> secondV = initialV;

    do
    {
        firstV = secondV;
        vector< vector<double> > matrix = getDerivativeValue(firstV);

        vector<double> freeColumn = getSystemFunctionValue(firstV);
        freeColumn[0] = - freeColumn[0];
        freeColumn[1] = - freeColumn[1];

        vector<double> dxSolution = GaussObject.getSolution(matrix, freeColumn, true);
        std::transform(firstV.begin(), firstV.end(), dxSolution.begin(), secondV.begin(), std::plus<double>());
    }
    while(sqrt(pow(secondV[0] - firstV[0], 2) + pow(secondV[1] - firstV[1], 2)) >= epsilon);

    return secondV;
};

vector<double> NewtonMethod::getSystemFunctionValue(vector<double> iteration)
{
    if (iteration.size() != 2)
        throw "Error: arguments vector size is not correct.";

    vector<double> value(2, 0);
    value[0] = cos(iteration[1]) + c * iteration[0];
    value[1] = a * iteration[0] + b * iteration[1] + pow(iteration[0], 2) * iteration[1];

    return value;
};

vector< vector<double> > NewtonMethod::getDerivativeValue(vector<double> iteration)
{
    if (iteration.size() != 2)
        throw "Error: arguments vector size is not correct.";

    vector< vector<double> > value(2, vector<double>(2, 0));
    value[0][0] = c;
    value[0][1] = - sin(iteration[1]);
    value[1][0] = a;
    value[1][1] = b + pow(iteration[0], 2);

    return value;
};