#ifndef NEWTONMETHOD_H
#define NEWTONMETHOD_H

// C++ standard includes
#include <vector>
using std::vector;

class NewtonMethod //!< Реализация метода Ньютона
{
private:
    //!< Коэффициенты для заданной функции
    int k, m, l;

    //!< Коэффициенты для заданной системы уравнений
    const double a = 0.24;
    const double b = 3.5;
    const double c = 2.0;
public:
    NewtonMethod();                                                            //!< Конструктор класса
    NewtonMethod(int k, int m, int l);                                         //!< Конструктор с параметрами

    double getSolution(double initialX, double epsilon);                       //!< Решение уравнения методом Ньютона
    double getFunctionValue(double x);                                         //!< Значение функции
    double getDerivativeValue(double x);                                       //!< Значение производной функции

    vector<double> getSystemSolution(vector<double> initialV, double epsilon); //!< Решение системы уравнений методом Ньютона
    vector<double> getSystemFunctionValue(vector<double> iteration);           //!< Значения функций системы уравнений
    vector< vector<double> > getDerivativeValue(vector<double> iteration);     //!< Значения функций phi(x) системы уравнений
};

#endif // NEWTONMETHOD_H