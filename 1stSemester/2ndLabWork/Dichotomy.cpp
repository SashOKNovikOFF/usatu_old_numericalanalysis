// Related headers
#include "Dichotomy.h"

// C++ standard includes
#include <cmath>

#include <vector>
using std::vector;

const double PI = 3.14;

Dichotomy::Dichotomy()
{
    k = 1;
    m = 1;
    l = 1;
};

Dichotomy::Dichotomy(int k, int m, int l)
{
    this->k = k;
    this->m = m;
    this->l = l;
};

double Dichotomy::getSolution(double epsilon)
{
    const int pointCount = 10;
    vector<double> xValues(pointCount, 0.0);
    vector<double> yValues(pointCount, 0.0);

    xValues[0] = 0.0;
    xValues[9] = 1.0;
    for (int i = 1; i < xValues.size() - 2; i++)
        xValues[i] = (i + 1) * (1.0 / pointCount);
    for (int i = 0; i < yValues.size(); i++)
        yValues[i] = getFunctionValue(xValues[i]);

    double tempXLeft, tempXRight;
    double tempYLeft, tempYRight;
    for (int i = 0; i < yValues.size() - 1; i++)
        if (yValues[i] * yValues[i + 1] < 0.0)
        {
            tempXLeft  = xValues[i];
            tempXRight = xValues[i + 1];
            tempYLeft  = yValues[i];
            tempYRight = yValues[i + 1];

            break;
        };

    double xCenter, yCenter;
    while (tempXRight - tempXLeft >= 2 * epsilon)
    {
        xCenter = (tempXLeft + tempXRight) / 2;
        yCenter = getFunctionValue(xCenter);

        if (tempYLeft * yCenter < 0.0)
        {
            tempXRight = xCenter;
            tempYRight = yCenter;
        }
        else if (yCenter * tempYRight < 0.0)
        {
            tempXLeft = xCenter;
            tempYLeft = yCenter;
        }
        else
            break;
    };

    return (tempXLeft + tempXRight) / 2;
};

double Dichotomy::getFunctionValue(double x)
{
    return k * cos(PI * pow(x, m)) + pow(x, l) * (1 - x);
};