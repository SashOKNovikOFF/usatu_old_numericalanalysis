reset

set xrange [0.0:1.0]

set key left top

set xlabel "t"
set ylabel "y(t)"

plot "3.txt" using 1:2 with points ps 2 ti "Original function", \
     "4.txt" using 1:2 with lines ti "Approximation function"