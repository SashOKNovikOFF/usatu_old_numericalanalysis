// Related headers
#include "TDMAlgorithm.h"

// C++ standard includes
#include <cmath>
using std::abs;

// User includes
#include "InputOutput.h"

TDMAlgorithm::TDMAlgorithm()
{

};

vector<double> TDMAlgorithm::getSolution(vector< vector<double> >& matrix)
{
    int size = matrix[0].size();

    vector<double> ksi(size + 1);
    vector<double> eta(size + 1);
    ksi[0] = eta[0] = 0.0;

    vector<double> x(size + 2);
    x[size + 1] = 0;

    for (int i = 0; i < size; i++)
    {
        ksi[i + 1] = matrix[2][i] / (matrix[1][i] - matrix[0][i] * ksi[i]);
        eta[i + 1] = (matrix[0][i] * eta[i] - matrix[3][i]) / (matrix[1][i] - matrix[0][i] * ksi[i]);
    };

    for (int i = size + 1; i > 1; i--)
        x[i - 1] = ksi[i - 1] * x[i] + eta[i - 1];

    x.erase(x.begin() + 0);
    x.erase(x.begin() + size);

    return x;
};

void TDMAlgorithm::getSLESolution(InputOutput& IOObject, char inputFlag, char outputFlag)
{
    vector< vector<double> > matrix;
    vector<double> freeColumn;

    if (inputFlag == 'c')
        IOObject.inputSLEConsole(matrix, freeColumn);
    else if (inputFlag == 'f')
        IOObject.inputSLEFile(matrix, freeColumn);
    else
        throw "Error: incorrect symbol in the 2nd parameter (must be 'c' or 'f').";

    // Проверка размеров матрицы и столбца свободных членов
    if (matrix.size() != freeColumn.size())
       throw "Error: sizes of matrix and free column aren't equal.";
    for (int row = 0; row < matrix.size(); row++)
        if (matrix.size() != matrix[row].size())
            throw "Error: matrix is not square.";

    // Проверка трёхдиагональности матрицы
    for (int row = 0; row < matrix.size() - 2; row++)
        for (int column = row + 2; column < matrix.size(); column++)
            if ((matrix[row][column] != 0) || (matrix[column][row] != 0))
                throw "Error: matrix is not tridiagonal.";

    // Проверка диагонального преобладания
    bool strictFlag = true;

    if ((abs(matrix[0][0]) < abs(matrix[0][1])) || (abs(matrix[matrix.size() - 1][matrix.size() - 2]) < abs(matrix[matrix.size() - 1][matrix.size() - 1])))
       throw "Error: there is no diagonally dominant matrix.";
    if ((abs(matrix[0][0]) != abs(matrix[0][1])) || (abs(matrix[matrix.size() - 1][matrix.size() - 2]) != abs(matrix[matrix.size() - 1][matrix.size() - 1])))
       strictFlag = false;

    for (int row = 1; row < matrix.size() - 1; row++)
    {
        if (abs(matrix[row][row]) < (abs(matrix[row][row - 1]) + abs(matrix[row][row + 1])))
            throw "Error: there is no diagonally dominant matrix.";
        if (abs(matrix[row][row]) == (abs(matrix[row][row - 1]) + abs(matrix[row][row + 1])))
            strictFlag = false;
    };

    if (strictFlag)
        throw "Error: there is no diagonally dominant matrix.";

    // Матрица ненулевых элементов трёхдиагональной матрицы
    vector< vector<double> > TDMAMatrix(4, vector<double>(matrix.size(), 0));

    // Ввод начальных значений в матрицу прогонки
    TDMAMatrix[0][0] = 0;
    TDMAMatrix[1][0] = -matrix[0][0];
    TDMAMatrix[2][0] = matrix[0][1];
    TDMAMatrix[3][0] = freeColumn[0];

    // Ввод конечных значений в матрицу прогонки
    TDMAMatrix[0][matrix.size() - 1] = matrix[matrix.size() - 1][matrix.size() - 2];
    TDMAMatrix[1][matrix.size() - 1] = -matrix[matrix.size() - 1][matrix.size() - 1];
    TDMAMatrix[2][matrix.size() - 1] = 0;
    TDMAMatrix[3][matrix.size() - 1] = freeColumn[matrix.size() - 1];

    for (int i = 1; i < matrix.size() - 1; i++)
    {
        TDMAMatrix[0][i] = matrix[i][i - 1];
        TDMAMatrix[1][i] = -matrix[i][i];
        TDMAMatrix[2][i] = matrix[i][i + 1];
        TDMAMatrix[3][i] = freeColumn[i];
    }

    vector<double> solution;
    solution = getSolution(TDMAMatrix);
    if (outputFlag == 'c')
        IOObject.outputSLESolutionConsole(solution);
    else if (outputFlag == 'f')
        IOObject.outputSLESolutionFile(solution);
    else
        throw "Error: incorrect symbol in the 3rd parameter (must be 'c' or 'f').";
};
