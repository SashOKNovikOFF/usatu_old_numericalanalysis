#ifndef ITERATIVEMETHOD_H
#define ITERATIVEMETHOD_H

//C++ standard includes
#include <vector>
using std::vector;

// User includes
#include "InputOutput.h"

class IterativeMethod //!< Реализация метода простых итераций
{
public:
    IterativeMethod();                                                                                                            //!< Конструктор класса
    vector<double> getSolution(vector< vector<double> >& matrix, vector<double>& freeColumn, double minLambda, double maxLambda); //!< Метод простых итераций

    // Функции, предназначенные для теста алгоритма
    void getSLESolution(InputOutput& IOObject, char outputFlag);                                                                  //!< Решение СЛУ (ввод/решение/вывод)
};

#endif // ITERATIVEMETHOD_H
