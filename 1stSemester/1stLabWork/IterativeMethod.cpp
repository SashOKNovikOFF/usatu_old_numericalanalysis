// Related headers
#include "IterativeMethod.h"

// C++ standard includes
#include <cmath>
#include <algorithm>
#include <functional>

// User includes
#include "InputOutput.h"

IterativeMethod::IterativeMethod()
{

};

vector<double> IterativeMethod::getSolution(vector< vector<double> >& matrix, vector<double>& freeColumn, double minLambda, double maxLambda)
{
    // Задаём начальные данные (начальный вектор; итерационный параметр tao)
    vector<double> lastVector = freeColumn;
    vector<double> currentVector(matrix.size(), 0);
    const double tao     =                     2.0 / (minLambda + maxLambda);
    const double epsilon = (maxLambda - minLambda) / (minLambda + maxLambda);

    // Для итерационного метода преобразуем столбец свободных членов
    vector<double> freeSummand(matrix.size(), 0);
    for (int row = 0; row < matrix.size(); row++)
        freeSummand[row] = tao * freeColumn[row];

    // Вводим временные переменные для итерационного процесса
    vector<double> tempVector(matrix.size(), 0);

    // Запускаем итерационный процесс
    int step = 1;
    while (true)
    {
        vector<double> tempSum(matrix.size(), 0);

        // Получаем следующий вектор
        std::transform(lastVector.begin(), lastVector.end(), currentVector.begin(), currentVector.begin(), std::plus<double>());
        for (int row = 0; row < matrix.size(); row++)
        {
            std::transform(lastVector.begin(), lastVector.end(), matrix[row].begin(), tempVector.begin(), std::multiplies<double>());

            for (int element = 0; element < lastVector.size(); element++)
                tempSum[row] += tempVector[element];
            tempSum[row] *= -tao;
        }
        std::transform(tempSum.begin(), tempSum.end(), currentVector.begin(), currentVector.begin(), std::plus<double>());
        std::transform(freeSummand.begin(), freeSummand.end(), currentVector.begin(), currentVector.begin(), std::plus<double>());

        // Вводим временные переменные для сходимости метода
        double tempNorm1 = 0.0;
        double tempNorm2 = 0.0;

        // Проверка сходимости решения
        std::transform(currentVector.begin(), currentVector.end(), lastVector.begin(), tempVector.begin(), std::minus<double>());
        for (int i = 0; i < tempVector.size(); i++)
            tempNorm1 += pow(tempVector[i], 2);
        tempNorm1 = pow(tempNorm1, 0.5);

        std::transform(currentVector.begin(), currentVector.end(), freeColumn.begin(), tempVector.begin(), std::minus<double>());
        for (int i = 0; i < tempVector.size(); i++)
            tempNorm2 += pow(tempVector[i], 2);
        tempNorm2 = pow(tempNorm2, 0.5);

        if (step != 1)
            if (tempNorm1 <= pow(epsilon, step) * tempNorm2)
                return currentVector;

        lastVector = currentVector;
        currentVector.clear();
        currentVector.resize(matrix.size());

        step++;
    };
};

void IterativeMethod::getSLESolution(InputOutput& IOObject, char outputFlag)
{
    double minLambda = 0.906;
    double maxLambda = 1.246;

    vector< vector<double> > matrix = { {    1, 0.03, 0.04, 0.05, 0.06 },
                                        { 0.03,    1, 0.05, 0.06, 0.07 },
                                        { 0.04, 0.05,    1, 0.07, 0.08 },
                                        { 0.05, 0.06, 0.07,    1, 0.09 },
                                        { 0.06, 0.07, 0.08, 0.09,    1 } };
    vector<double> freeColumn = { 4.5, 9, 13.5, 18, 22.5 };

    // Проверка размеров матрицы и столбца свободных членов
    if (matrix.size() != freeColumn.size())
       throw "Error: sizes of matrix and free column aren't equal.";
    for (int row = 0; row < matrix.size(); row++)
        if (matrix.size() != matrix[row].size())
            throw "Error: matrix is not square.";

    // Проверка положительности диагональных элементов
    for (int i = 0; i < matrix.size(); i++)
        if (matrix[i][i] <= 0)
            throw ("Error: there is element < 0.");

    // Проверка симметричности матрицы
    for (int row = 0; row < matrix.size() - 1; row++)
        for (int column = row + 1; column < matrix.size(); column++)
            if (matrix[row][column] != matrix[column][row])
                throw "Error: matrix is not symmetric.";

    // Проверка строгого диагонального преобладания
    for (int row = 0; row < matrix.size(); row++)
    {
        double tempSum = 0.0;
        for (int column = 0; column < matrix.size(); column++)
            if (column != row)
                tempSum += abs(matrix[row][column]);

        if (matrix[row][row] <= tempSum)
            throw "Error: there is no diagonally dominant matrix.";
    };

    vector<double> solution;
    solution = getSolution(matrix, freeColumn, minLambda, maxLambda);
    if (outputFlag == 'c')
        IOObject.outputSLESolutionConsole(solution);
    else if (outputFlag == 'f')
        IOObject.outputSLESolutionFile(solution);
    else
        throw "Error: incorrect symbol in the 2nd parameter (must be 'c' or 'f').";
};