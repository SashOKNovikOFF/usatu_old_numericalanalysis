// C++ standard includes
#include <iostream>

// User includes
#include "InputOutput.h"
#include "TDMAlgorithm.h"
#include "GaussAlgorithm.h"
#include "IterativeMethod.h"

int main()
{
    try
    {
        TDMAlgorithm TDMObject;
        GaussAlgorithm GaussObject;
        IterativeMethod IterativeObject;

        InputOutput IOObject;
        std::cout << "TDM algorithm. ";
        TDMObject.getSLESolution(IOObject, 'f', 'f');
        std::cout << std::endl << "Gauss algorithm. ";
        GaussObject.getSLESolution(IOObject, true, 'f', 'f');
        std::cout << std::endl << "Iterative method. ";
        IterativeObject.getSLESolution(IOObject, 'f');

        return 0;
    }
    catch(char const* error)
    {
        std::cout << error << std::endl;
        return 1;
    };
};
