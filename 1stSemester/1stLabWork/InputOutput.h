#ifndef INPUTOUTPUT_H
#define INPUTOUTPUT_H

// C++ standard includes
#include <string>
#include <vector>

using std::string;
using std::vector;

class InputOutput //!< Класс ввода/вывода данных по системам линейных уравнений
{
private:
    std::string inputFileName;                                                            //!< Название входного файла
    std::string outputFileName;                                                           //!< Название выходного файла
public:
    InputOutput();                                                                        //!< Конструктор класса
    InputOutput(string inputFileName, string outputFileName);                             //!< Конструктор с параметрами

    void inputSLEConsole(vector< vector<double> > & matrix, vector<double> & freeColumn); //!< Ввод системы линейных уравнений через консоль
    void inputSLEFile(vector< vector<double> > & matrix, vector<double> & freeColumn);    //!< Ввод системы линейных уравнений через файл

    void outputSLESolutionConsole(vector<double> & solution);                             //!< Вывод решения системы линейных уравнений через консоль
    void outputSLESolutionFile(vector<double> & solution);                                //!< Вывод решения системы линейных уравнений через файл
};

#endif // INPUTOUTPUT_H
