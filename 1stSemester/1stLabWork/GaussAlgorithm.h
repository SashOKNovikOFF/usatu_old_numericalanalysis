#ifndef GAUSSALGORITHM_H
#define GAUSSALGORITHM_H

// C++ standard includes
#include <vector>
using std::vector;

// User includes
#include "InputOutput.h"

class GaussAlgorithm //!< Реализация метода Гаусса с выбором главных элементов
{
public:
    GaussAlgorithm();                                                                                         //!< Конструктор класса
    vector<double> getSolution(vector< vector<double> >& matrix, vector<double>& freeColumn, bool direction); //!< Метод Гаусса с выбором главного элемента

    // Функции, предназначенные для теста алгоритма
    void getSLESolution(InputOutput& IOObject, bool direction, char inputFlag, char outputFlag);              //!< Решение СЛУ (ввод/решение/вывод)
};

#endif // GAUSSALGORITHM_H
