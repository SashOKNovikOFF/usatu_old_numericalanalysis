#ifndef TDMALGORITHM_H
#define TDMALGORITHM_H

// C++ standard includes
#include <vector>
using std::vector;

// User includes
#include "InputOutput.h"

class TDMAlgorithm //!< Реализация метода прогонки
{
public:
    TDMAlgorithm();                                                              //!< Конструктор класса
    vector<double> getSolution(vector< vector<double> >& matrix);                //!< Метод прогонки

    // Функции, предназначенные для теста алгоритма
    void getSLESolution(InputOutput& IOObject, char inputFlag, char outputFlag); //!< Решение СЛУ (ввод/решение/вывод)
};

#endif // TDMALGORITHM_H
