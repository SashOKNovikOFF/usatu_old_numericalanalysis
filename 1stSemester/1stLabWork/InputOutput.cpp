// C++ standard includes
#include <iostream>
#include <fstream>
#include <cstring>

// User includes
#include "InputOutput.h"

InputOutput::InputOutput()
{
    this->inputFileName  = "input.txt";
    this->outputFileName = "output.txt";
};

InputOutput::InputOutput(string inputFileName, string outputFileName)
{
    this->inputFileName  = inputFileName;
    this->outputFileName = outputFileName;
};

void InputOutput::inputSLEConsole(vector< vector<double> > & matrix, vector<double> & freeColumn)
{
    int weightMatrix;
    int lengthMatrix;

    // Ввод размера матрицы
    std::cout << "Input size of matrix [M, N]: ";
    std::cin >> weightMatrix >> lengthMatrix;

    // Изменение размеров матрицы и столбца свободных членов
    matrix.resize(weightMatrix);
    for (int i = 0; i < weightMatrix; i++)
        matrix[i].resize(lengthMatrix);
    freeColumn.resize(weightMatrix);

    // Ввод элементов матрицы
    for (int i = 0; i < weightMatrix; i++)
        for (int j = 0; j < lengthMatrix; j++)
        {
            std::cout << "Input element of matrix [" << i + 1 << ", " << j + 1 << "]: ";
            std::cin >> matrix[i][j];
        }

    // Ввод элементов столбца свободных членов
    for (int i = 0; i < weightMatrix; i++)
    {
        std::cout << "Input element of free column [" << i + 1 << "]: ";
        std::cin >> freeColumn[i];
    }
};

void InputOutput::inputSLEFile(vector< vector<double> > & matrix, vector<double> & freeColumn)
{
    std::fstream fileStream(inputFileName.c_str(), std::fstream::in);
    if (!fileStream.is_open())
       throw "File doesn't exist!";

    int weightMatrix = 0;
    int lengthMatrix = 0;

    // Ввод размера матрицы
    fileStream >> weightMatrix;
    fileStream >> lengthMatrix;

    // Изменение размеров матрицы и столбца свободных членов
    matrix.resize(weightMatrix);
    for (int i = 0; i < weightMatrix; i++)
        matrix[i].resize(lengthMatrix);
    freeColumn.resize(weightMatrix);

    // Ввод элементов матрицы
    for (int i = 0; i < weightMatrix; i++)
        for (int j = 0; j < lengthMatrix; j++)
            fileStream >> matrix[i][j];

    // Ввод элементов столбца свободных членов
    for (int i = 0; i < weightMatrix; i++)
        fileStream >> freeColumn[i];

    fileStream.close();
};

void InputOutput::outputSLESolutionConsole(vector<double> & solution)
{
    std::cout << "Solution of SLE:" << std::endl;
    for (int i = 0; i < solution.size() - 1; i++)
        std::cout << "x[" << i + 1 << "] = " << solution[i] << ";" << std::endl;
    std::cout << "x[" << solution.size() << "] = " << solution[solution.size() - 1] << "." << std::endl;
};

void InputOutput::outputSLESolutionFile(vector<double> & solution)
{
    std::fstream fileStream(outputFileName.c_str(), std::fstream::out);
    if (!fileStream.is_open())
       throw "File doesn't exist!";

    fileStream << "Solution of SLE:" << std::endl;
    for (int i = 0; i < solution.size() - 1; i++)
        fileStream << "x[" << i + 1 << "] = " << solution[i] << ";" << std::endl;
    fileStream << "x[" << solution.size() << "] = " << solution[solution.size() - 1] << "." << std::endl;

    fileStream.close();
};