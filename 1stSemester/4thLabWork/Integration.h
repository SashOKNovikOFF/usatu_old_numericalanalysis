#ifndef INTEGRATION_H
#define INTEGRATION_H

class Integration //!< Реализация методов интегрирования
{
public:
    Integration();                        //!< Конструктор класса

    double getFunctionValue(double x);    //!< Значение заданной функции
    
    double getIntLeftRect(int nodes);     //!< Формула левых прямоугольников
    double getIntRightRect(int nodes);    //!< Формула правых прямоугольников
    double getIntAverageRect(int nodes);  //!< Формула средних прямоугольников
    
    double getIntTrapeze(int nodes);      //!< Формула трапеций
    double getIntSimpson(int nodes);      //!< Формула Симпсона
    double getCheckRunge(double epsilon); //!< Поправка Рунге для формулы Симпсона
    
    double getGaussFuncValue(double t);   //!< Значение функции f(t)
    double getIntGauss(int nodes);        //!< Квадратурная формула Гаусса
};

#endif // INTEGRATION_H