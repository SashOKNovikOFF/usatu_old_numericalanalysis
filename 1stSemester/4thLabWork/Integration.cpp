// Related headers
#include "Integration.h"

// C++ standard includes
#include <iostream>
#include <cmath>

const double PI = 3.14159;

Integration::Integration()
{

};


double Integration::getFunctionValue(double x)
{
    return cos(pow(x, 3.0));
};

double Integration::getIntLeftRect(int nodes)
{
    double step = 1.0 / nodes;
    
    double sum = 0.0;
    for (int i = 0; i < nodes - 1; i++)
        sum += step * getFunctionValue(i * step);
    
    return sum;
};

double Integration::getIntRightRect(int nodes)
{
    double step = 1.0 / nodes;
    
    double sum = 0.0;
    for (int i = 0; i < nodes - 1; i++)
        sum += step * getFunctionValue((i + 1) * step);
    
    return sum;
};

double Integration::getIntAverageRect(int nodes)
{
    double step = 1.0 / nodes;
    
    double sum = 0.0;
    for (int i = 0; i < nodes - 1; i++)
        sum += step * getFunctionValue((2 * i + 1) * step / 2);
    
    return sum;
};

double Integration::getIntTrapeze(int nodes)
{
    double step = 1.0 / nodes;
    
    double sum = 0.0;
    for (int i = 0; i < nodes - 1; i++)
        sum += step / 2 * (getFunctionValue((i + 1) * step) + getFunctionValue(i * step));
    
    return sum;
};

double Integration::getIntSimpson(int nodes)
{
    double step = 1.0 / nodes;
    
    double sum = 0.0;
    for (int i = 0; i < nodes - 1; i++)
        sum += step / 6 * (getFunctionValue(i * step) + 4 * getFunctionValue((2 * i + 1) * step / 2) + getFunctionValue((i + 1) * step));
    
    return sum;
};

double Integration::getCheckRunge(double epsilon)
{
    int nodes = 10;
    double SimpsonFirst = getIntSimpson(nodes);
    nodes *= 2;
    double SimpsonSecond = getIntSimpson(nodes);
    
    while ((1.0 / 15.0) * (SimpsonSecond - SimpsonFirst) >= epsilon)
    {
        nodes *= 2;
        SimpsonFirst = SimpsonSecond;
        SimpsonSecond = getIntSimpson(nodes);
    };
    
    return SimpsonFirst + (1.0 / 15.0) * (SimpsonFirst - SimpsonSecond);
};

double Integration::getGaussFuncValue(double t)
{
    return cos(0.5 + 0.5 * t) * sqrt(1 - t * t) / (1.5 + 0.5 * t);
};

double Integration::getIntGauss(int nodes)
{
    double sum = 0.0;
    
    for (int k = 1; k <= nodes; k++)
        sum += getGaussFuncValue(cos((2 * k - 1) * PI / 2 / nodes));
    
    return 0.5 * PI / nodes * sum;
};