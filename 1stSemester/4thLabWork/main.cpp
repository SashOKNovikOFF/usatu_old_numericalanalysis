// C++ standard includes
#include <iostream>

#include <vector>
using std::vector;

// User includes
#include "Integration.h"

int main()
{
    try
    {
        Integration IntObject;
        
        std::cout << "Left rectangle method: " << IntObject.getIntLeftRect(2000) << std::endl;
        std::cout << "Right rectangle method: " << IntObject.getIntRightRect(2000) << std::endl;
        std::cout << "Average rectangle method: " << IntObject.getIntAverageRect(2000) << std::endl;
        
        std::cout << "Trapeze method: " << IntObject.getIntTrapeze(2000) << std::endl;
        
        std::cout << "Simpson's method: " << IntObject.getIntSimpson(2000) << std::endl;
        std::cout << "Runge's check: " << IntObject.getCheckRunge(0.000001) << std::endl;
        
        std::cout << "Gauss method: " << IntObject.getIntGauss(1000) << std::endl;
        
        return 0;
    }
    catch(char const* error)
    {
        std::cout << error << std::endl;
        return 1;
    };
};
