// Related headers
#include "Interpolation.h"

// C++ standard includes
#include <iostream>
#include <cmath>

Interpolation::Interpolation()
{
    a = 1;
    b = 1;
    k = 1;
    m = 1;
    
    double step = 1.0 / N;
    for (double i = 0; i < N; i++)
    {
        xValues.push_back(i * step);
        yValues.push_back(getFunctionValue(i * step));
    };
};

Interpolation::Interpolation(int a, int b, int k, int m)
{
    this->a = a;
    this->b = b;
    this->k = k;
    this->m = m;
    
    double step = 1.0 / N;
    for (double i = 0; i < N; i++)
    {
        xValues.push_back(i * step);
        yValues.push_back(getFunctionValue(i * step));
    };
};

double Interpolation::getFunctionValue(double x)
{
    return pow(x, k) / (a + b * pow(x, m));
};

void Interpolation::outputFValues()
{
    std::cout << "Table of function y(x) [x, y(x)]:" << std::endl;
    for (int i = 0; i < xValues.size(); i++)
        std::cout << xValues[i] << " " << yValues[i] << std::endl;
};

void Interpolation::getInterpolation()
{
    for (int i = 0; i < yValues.size() - 1; i++)
        ai.push_back(yValues[i]);
    
    // Трёхдиагональная матрица
    vector< vector<double> > TDMAMatrix(4, vector<double>(N - 1, 0));
    double step = 1.0 / N;
    
    // Левое граничное условие
    TDMAMatrix[0][0] = 0;
    TDMAMatrix[1][0] = 4 * step;
    TDMAMatrix[2][0] = step;
    TDMAMatrix[3][0] = 3 * ((yValues[2] - yValues[1]) / step - (yValues[1] - yValues[0]) / step);
    
    // Правое граничное условие
    TDMAMatrix[0][N - 2] = step;
    TDMAMatrix[1][N - 2] = 4 * step;
    TDMAMatrix[2][N - 2] = 0;
    TDMAMatrix[3][N - 2] = 3 * ((yValues[N] - yValues[N - 1]) / step - (yValues[N - 1] - yValues[N - 2]) / step);
    
    // Значения на узлах
    for (int i = 1; i < (N - 2); i++)
    {
        TDMAMatrix[0][i] = step;
        TDMAMatrix[1][i] = 4 * step;
        TDMAMatrix[2][i] = step;
        TDMAMatrix[3][i] = 3 * ((yValues[i + 2] - yValues[i + 1]) / step - (yValues[i + 1] - yValues[i]) / step);
    };
    
    ci = TDMObject.getSolution(TDMAMatrix);
    ci.insert(ci.begin(), 0.0);
    
    for (int i = 0; i < N - 1; i++)
        bi.push_back((yValues[i + 1] - yValues[i]) / step - (1.0 / 3.0) * step * (ci[i + 1] + ci[i]));
    bi.push_back((yValues[N] - yValues[N - 1]) / step - (2.0 / 3.0) * step * ci[N - 1]);
    
    for (int i = 0; i < N - 1; i++)
        di.push_back((ci[i + 1] - ci[i]) / 3 / step);
    di.push_back(- ci[N - 1] / 3 / step);
};

double Interpolation::getInterpolationValue(double x)
{
    int index = 0;
    while((xValues[index + 1] < x) && (index < xValues.size() - 1))
        index++;
    
    double length = x - xValues[index];
    return ai[index] + bi[index] * length + ci[index] * pow(length, 2) + di[index] * pow(length, 3);
};

void Interpolation::outputIValues(int points)
{
    if (ai.size() == 0)
        getInterpolation();
    
    std::cout << "Table of function phi(x) [x, phi(x)]:" << std::endl;
    double step = 1.0 / points;
    for (double i = 0; i < points; i++)
        if (i * step < xValues[N - 1])
            std::cout << i * step << " " << getInterpolationValue(i * step) << std::endl;
};