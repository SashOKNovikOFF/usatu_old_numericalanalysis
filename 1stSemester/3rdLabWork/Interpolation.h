#ifndef INTERPOLATION_H
#define INTERPOLATION_H

// C++ standard includes
#include <vector>
using std::vector;

// User headers
#include "TDMAlgorithm.h"

class Interpolation //!< Реализация интерполяции
{
private:
    int a, b, k, m;                            //!< Коэффициенты заданной функции
    const int N = 50;                          //!< Количество точек заданной функции [0:N]
    vector<double> xValues, yValues;           //!< Координаты точек заданной функции
    
    vector<double> ai, bi, ci, di;             //!< Коэффициенты интерполяционной функции
    TDMAlgorithm TDMObject;                    //!< Метод прогонки
public:
    Interpolation();                           //!< Конструктор класса
    Interpolation(int a, int b, int k, int m); //!< Конструктор с параметрами

    double getFunctionValue(double x);         //!< Значение заданной функции
    void outputFValues();                      //!< Вывод координат точек заданной функции
    
    void getInterpolation();                   //!< Создание интерполяционной функции
    double getInterpolationValue(double x);    //!< Значение интерполяционной функции
    void outputIValues(int points);            //!< Вывод координат точек интерполяционной функции
};

#endif // INTERPOLATION_H