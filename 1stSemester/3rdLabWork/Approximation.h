#ifndef APPROXYMATION_H
#define APPROXYMATION_H

// C++ standard includes
#include <vector>
using std::vector;

// User headers
#include "GaussAlgorithm.h"

class Approximation //!< Реализация аппроксимации
{
private:
    int q;                                  //!< Коэффициенты заданной функции
    const int N = 25;                       //!< Количество точек заданной функции [0:N]
    vector<double> xValues, yValues;        //!< Координаты точек заданной функции
    
    int power;                              //!< Число членов функции-аппроксимации
    vector<double> ak;                      //!< Коэффициенты функции-аппроксимации
    const double epsilon = 0.01;            //!< Погрешность эксперимента 
    GaussAlgorithm GaussObject;             //!< Алгоритм решения СЛУ методом Гаусса
public:
    Approximation();                        //!< Конструктор класса
    Approximation(int q);                   //!< Конструктор с параметрами

    double getFunctionValue(double x);      //!< Значение заданной функции
    void outputFValues();                   //!< Вывод координат точек заданной функции
    
    void getApproximation();                //!< Создание функции-аппроксимации
    double getApproximationValue(double x); //!< Значение функции-аппроксимации
    void chooseBestApproximation();         //!< Выбор наилучшего приближения
    void outputAValues(int points);         //!< Вывод координат точек функции-аппроксимации
};

#endif // APPROXYMATION_H