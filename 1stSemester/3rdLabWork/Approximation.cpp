// Related headers
#include "Approximation.h"

// C++ standard includes
#include <iostream>
#include <cmath>

Approximation::Approximation()
{
    q = 1;
    power = 1;
    
    double step = 1.0 / N;
    for (double i = 0; i < N; i++)
    {
        xValues.push_back(i * step);
        yValues.push_back(getFunctionValue(i * step));
    };
};

Approximation::Approximation(int q)
{
    this->q = q;
    power = 1;
    
    double step = 1.0 / N;
    for (double i = 0; i < N; i++)
    {
        xValues.push_back(i * step);
        yValues.push_back(getFunctionValue(i * step));
    };
};

double Approximation::getFunctionValue(double x)
{
    return pow(1 + pow(x, q), 0.5);
};

void Approximation::outputFValues()
{
    std::cout << "Table of function y(x) [x, y(x)]:" << std::endl;
    for (int i = 0; i < xValues.size(); i++)
        std::cout << xValues[i] << " " << yValues[i] << std::endl;
};

void Approximation::getApproximation()
{
    vector< vector<double> > matrix(power + 1, vector<double>(power + 1, 0));
    vector<double> freeColumn(power + 1, 0);
    
    for (int row = 0; row < matrix.size(); row++)
        for (int column = 0; column < matrix.size(); column++)
        {
            double temp = 0;
            for (int i = 0; i < N; i++)
               temp += pow(xValues[i], row + column);
            
            matrix[row][column] = temp;
        };
    
    for (int row = 0; row < matrix.size(); row++)
    {
        double temp = 0;
        for (int i = 0; i < N; i++)
           temp += yValues[i] * pow(xValues[i], row);
        
        freeColumn[row] = temp;
    };
    
    ak = GaussObject.getSolution(matrix, freeColumn, true);
};

double Approximation::getApproximationValue(double x)
{
    double sum = 0.0;
    for (int i = 0; i < ak.size(); i++)
        sum += ak[i] * pow(x, i);
    
    return sum;
};

void Approximation::chooseBestApproximation()
{
    bool flag = true;
    while (flag)
    {
        getApproximation();
        
        flag = false;
        for (int i = 0; i < N; i++)
        {
            if (std::abs(yValues[i] - getApproximationValue(xValues[i])) >= epsilon)
            {
                power++;
                flag = true;
            };
        };  
    };
};

void Approximation::outputAValues(int points)
{
    if (ak.size() == 0)
        chooseBestApproximation();
    
    std::cout << "Table of function phi(x) (power - " << power << "):" << std::endl;
    double step = 1.0 / points;
    for (double i = 0; i < points; i++)
        std::cout << i * step << " " << getApproximationValue(i * step) << std::endl;
};