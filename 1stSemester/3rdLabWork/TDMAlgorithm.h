#ifndef TDMALGORITHM_H
#define TDMALGORITHM_H

// C++ standard includes
#include <vector>
using std::vector;

class TDMAlgorithm //!< Реализация метода прогонки
{
public:
    TDMAlgorithm();                                               //!< Конструктор класса
    vector<double> getSolution(vector< vector<double> >& matrix); //!< Метод прогонки
};

#endif // TDMALGORITHM_H