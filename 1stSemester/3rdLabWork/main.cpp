// C++ standard includes
#include <iostream>

#include <vector>
using std::vector;

// User includes
#include "Interpolation.h"
#include "Approximation.h"

int main()
{
    Interpolation IPObject;
    Approximation APObject;
    try
    {
        IPObject.outputFValues();
        IPObject.outputIValues(50);
        
        APObject.outputFValues();
        APObject.outputAValues(50);

        return 0;
    }
    catch(char const* error)
    {
        std::cout << error << std::endl;
        return 1;
    };
};
