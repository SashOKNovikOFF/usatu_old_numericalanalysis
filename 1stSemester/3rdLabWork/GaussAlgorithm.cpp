// Related headers
#include "GaussAlgorithm.h"

// C++ standard includes
#include <algorithm>

GaussAlgorithm::GaussAlgorithm()
{

};

vector<double> GaussAlgorithm::getSolution(vector< vector<double> >& matrix, vector<double>& freeColumn, bool direction)
{
    // Матрица индексов, требуемая для выбора главного элемента по столбцам
    vector< vector<int> > indexes(2, vector<int>());

    // Проходим по всем столбцам
    for (int index = 0; index < matrix.size(); index++)
    {
        // Выбор главного элемента
        if (matrix[index][index] == 0)
        {
            if (direction == false) // По столбцам
            {
                int maxIndex = index;

                // Находим максимальный элемент в строке, перебирая столбцы
                for (int column = index; column < matrix.size(); column++)
                    if (abs(matrix[index][maxIndex]) < abs(matrix[index][column]))
                        maxIndex = column;

                // Запоминаем индексы столбцов, которые будем переставлять
                indexes[0].push_back(index);
                indexes[1].push_back(maxIndex);

                // Меняем столбцы местами
                for (int row = 0; row < matrix.size(); row++)
                    std::swap(matrix[row][index], matrix[row][maxIndex]);
            }
            else if (direction == true) // По строкам
            {
                int maxIndex = index;

                // Находим максимальный элемент в столбце, перебирая строки
                for (int row = 0; row < matrix.size(); row++)
                    if (abs(matrix[maxIndex][index]) < abs(matrix[row][index]))
                        maxIndex = row;

                // Меняем строки местами
                std::swap(matrix[index], matrix[maxIndex]);
                std::swap(freeColumn[index], freeColumn[maxIndex]);
            };
        };

        // Деление элементов строки на элемент, находящийся на главной диагонали
        for (int column = index + 1; column < matrix.size(); column++)
            matrix[index][column] = matrix[index][column] / matrix[index][index];
        freeColumn[index] /= matrix[index][index];
        matrix[index][index] = 1;


        // Исключение остальных элементов из текущего столбца
        for (int row = 0; row < matrix.size(); row++)
        {
            if (row != index)
            {
                for (int column = matrix.size() - 1; column > index; column--)
                {
                    matrix[row][column] -= matrix[row][index] * matrix[index][column];
                }
                freeColumn[row] -= matrix[row][index] * freeColumn[index];
                matrix[row][index] -= matrix[row][index] * matrix[index][index];
            };
        };
    };

    // Меняем столбцы обратно, если выборка главного элемента происходила по ним
    if (!indexes[0].empty())
        for (int i = indexes[0].size() - 1; i >= 0; i--)
            std::swap(freeColumn[ indexes[0][i] ], freeColumn[ indexes[1][i] ]);

    return freeColumn;
};