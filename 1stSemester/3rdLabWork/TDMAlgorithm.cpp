// Related headers
#include "TDMAlgorithm.h"

// C++ standard includes
#include <cmath>
using std::abs;

TDMAlgorithm::TDMAlgorithm()
{

};

vector<double> TDMAlgorithm::getSolution(vector< vector<double> >& matrix)
{
    int size = matrix[0].size();

    vector<double> ksi(size + 1);
    vector<double> eta(size + 1);
    ksi[0] = eta[0] = 0.0;

    vector<double> x(size + 2);
    x[size + 1] = 0;

    for (int i = 0; i < size; i++)
    {
        ksi[i + 1] = matrix[2][i] / (- matrix[1][i] - matrix[0][i] * ksi[i]);
        eta[i + 1] = (matrix[0][i] * eta[i] - matrix[3][i]) / (- matrix[1][i] - matrix[0][i] * ksi[i]);
    };
    
    for (int i = size + 1; i > 1; i--)
        x[i - 1] = ksi[i - 1] * x[i] + eta[i - 1];

    x.erase(x.begin() + 0);
    x.erase(x.begin() + size);

    return x;
};