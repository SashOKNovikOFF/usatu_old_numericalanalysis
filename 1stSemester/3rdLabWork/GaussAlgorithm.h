#ifndef GAUSSALGORITHM_H
#define GAUSSALGORITHM_H

// C++ standard includes
#include <vector>
using std::vector;

class GaussAlgorithm //!< Реализация метода Гаусса с выбором главных элементов
{
public:
    GaussAlgorithm();                                                                                         //!< Конструктор класса
    vector<double> getSolution(vector< vector<double> >& matrix, vector<double>& freeColumn, bool direction); //!< Метод Гаусса с выбором главного элемента
};

#endif // GAUSSALGORITHM_H
